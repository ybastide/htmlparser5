﻿using System.Collections.Generic;
using System.Text;
using System.IO;

namespace HtmlParser5
{
    /// <summary>
    /// Buffer with some interpreting and put-back.
    /// </summary>
    public sealed class HtmlCookedBuffer
    {
        public const int EOF = -1;

        readonly HtmlBuffer _buffer;

        /// <summary>
        /// The last 8 characters (somewhat).
        /// </summary>
        readonly List<int> _lastChars = new List<int>(8);

        /// <summary>
        /// Put-back buffer.
        /// </summary>
        readonly Stack<int> _putbackBuffer = new Stack<int>();

        ///// <summary>
        ///// Second surrogate for an utf32 character.
        ///// </summary>
        //char _surrogate;

        public HtmlCookedBuffer(Stream stream, Encoding encoding)
        {
            var hsb = new HtmlSimpleBuffer(stream);
            encoding = EncodingFinder.SniffEncoding(hsb, encoding);
            _buffer = new HtmlBuffer(hsb, encoding);
        }

        /// <summary>
        /// Returns the next character.
        /// </summary>
        /// <returns></returns>
        public int Get()
        {
            int c;
            if (_putbackBuffer.Count > 0)
                c = _putbackBuffer.Pop();
            //else if (_surrogate != 0)
            //{
            //    c = _surrogate;
            //    _surrogate = (char)0;
            //}
            else
                c = _buffer.Get();

            if (c == HtmlBuffer.EOF)
                return EOF;

            //if (c >= 0x10000)
            //{
            //    c -= 0x10000;
            //    int v1 = (c >> 10) | 0xD800;
            //    int v2 = (c & 0x3ff) | 0xDC00;
            //    c = v1;
            //    _surrogate = (char)v2;
            //}
            //else
            {
                if (c == '\r')
                {
                    c = _buffer.Peek() == '\n' ? _buffer.Get() : '\n';
                }
            }
            if (_lastChars.Count > 8)
                _lastChars.RemoveRange(0, 1);
            _lastChars.Add((char)c);
            return c;
        }

        /// <summary>
        /// Put back a character. 8 max.
        /// </summary>
        /// <param name="c"></param>
        public void Put(int c)
        {
            _putbackBuffer.Push(c);
            if (_lastChars.Count > 8)
                _lastChars.RemoveRange(0, 1);
            _lastChars.Add(c);
        }

        /// <summary>
        /// Did the stream end with the specified string?
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool EndsWith(string s)
        {
            int len = s.Length;
            int start = _lastChars.Count - len;
            if (start < 0)
                return false;
            for (int i = 0; i < len; i++)
                if (_lastChars[start + i] != s[i])
                    return false;
            return true;
        }

        /// <summary>
        /// Put back a string.
        /// </summary>
        /// <param name="s"></param>
        public void Put(string s)
        {
            if (string.IsNullOrEmpty(s))
                return;
            int len = s.Length;
            for (int i = len - 1; i >= 0; i--)
                _putbackBuffer.Push(s[i]);
            foreach (char c in s)
                _lastChars.Add(c);
            if (_lastChars.Count > 8)
                _lastChars.RemoveRange(0, _lastChars.Count - 8);
        }
    }
}
