﻿namespace HtmlParser5
{
    /// <summary>
    /// Tokenizer states.
    /// </summary>
    public enum TokenizerState
    {
        Data,
        CharacterReference,
        TagOpen,
        CloseTagOpen,
        TagName,
        BeforeAttributeName,
        AttributeName,
        AfterAttributeName,
        BeforeAttributeValue,
        AttributeValueDoubleQuoted,
        AttributeValueSingleQuoted,
        AttributeValueUnquoted,
        //CharacterReferenceInAttributeValue,
        AfterAttributeValueQuoted,
        SelfClosingStartTag,
        BogusComment,
        MarkupDeclarationOpen,
        CommentStart,
        CommentStartDash,
        Comment,
        CommentEndDash,
        CommentEnd,
        CommentEndBang,
        CommentEndSpace,
// ReSharper disable InconsistentNaming
        DOCTYPE,
        BeforeDOCTYPEName,
        DOCTYPEName,
        AfterDOCTYPEName,
        AfterDOCTYPEPublicKeyword,
        BeforeDOCTYPEPublicIdentifier,
        DOCTYPEPublicIdentifierDoubleQuoted,
        DOCTYPEPublicIdentifierSingleQuoted,
        AfterDOCTYPEPublicIdentifier,
        AfterDOCTYPESystemKeyword,
        BeforeDOCTYPESystemIdentifier,
        DOCTYPESystemIdentifierDoubleQuoted,
        DOCTYPESystemIdentifierSingleQuoted,
        AfterDOCTYPESystemIdentifier,
        BogusDOCTYPE,
        BetweenDOCTYPEPublicAndSystemIdentifiers,
        CDATASection,
        // RAWTEXT également
        RCDATALessThan,
        RCDATAEndTagOpen,
        RCDATAEndTagName,
        // ReSharper restore InconsistentNaming
        EOF,
    }
}