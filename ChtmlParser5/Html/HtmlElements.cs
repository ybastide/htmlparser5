﻿using HtmlParser5.Dom;

namespace HtmlParser5.Html
{
    public class Element : Node
    {
        public bool ParserInserted;

        protected Element()
        {
        }

#if false
        protected Element(Token.StartTag startTag)
            : base(startTag.Attributes)
        {
        }

        protected Element(List<Token.Tag.Attribute> attrs)
            : base(attrs)
        {
        }
#endif

        public Element SetAttributes(Token.StartTag startTag)
        {
            if (startTag != null)
                SetAttributes(startTag.Attributes);
            return this;
        }

        public override Node CloneNode()
        {
            var n = new Element {Attributes = Attributes};
            return n;
        }
    }

    public class Unknown : Element
    {
    }

    public class DocumentType : Node
    {
        private readonly Token.DocType _dt;

        public DocumentType(Token.DocType dt)
        {
            _dt = dt;
        }
    }

    public interface ISpecial
    {
    }

    public class Address : Element, ISpecial
    {
    }

    public class Area : Element, ISpecial
    {
    }

    public class Article : Element, ISpecial
    {
    }

    public class Aside : Element, ISpecial
    {
    }

    public class Base : Element, ISpecial
    {
    }

    public class Basefont : Element, ISpecial
    {
    }

    public class Bgsound : Element, ISpecial
    {
    }

    public class Blockquote : Element, ISpecial
    {
    }

    public class Body : Element, ISpecial
    {
    }

    public class Br : Element, ISpecial
    {
    }

    public class Center : Element, ISpecial
    {
    }

    public class Col : Element, ISpecial
    {
    }

    public class Colgroup : Element, ISpecial
    {
    }

    public class Command : Element, ISpecial
    {
    }

    public class Datagrid : Element, ISpecial
    {
    }

    public class Dd : Element, ISpecial
    {
    }

    public class Details : Element, ISpecial
    {
    }

    public class Dialog : Element, ISpecial
    {
    }

    public class Dir : Element, ISpecial
    {
    }

    public class Div : Element, ISpecial
    {
    }

    public class Dl : Element, ISpecial
    {
    }

    public class Dt : Element, ISpecial
    {
    }

    public class Embed : Element, ISpecial
    {
    }

    public class Fieldset : Element, ISpecial
    {
    }

    public class Figure : Element, ISpecial
    {
    }

    public class Footer : Element, ISpecial
    {
    }

    public class Form : Element, ISpecial
    {
    }

    public class Frame : Element, ISpecial
    {
    }

    public class Frameset : Element, ISpecial
    {
    }

    public interface IH : ISpecial
    {
    }

    public class H1 : Element, IH
    {
    }

    public class H2 : Element, IH
    {
    }

    public class H3 : Element, IH
    {
    }

    public class H4 : Element, IH
    {
    }

    public class H5 : Element, IH
    {
    }

    public class H6 : Element, IH
    {
    }

    public class Head : Element, ISpecial
    {
#if false
        public Head()
        {
        }

        public Head(Token.StartTag t)
            : base(t)
        {
        }
#endif
    }

    public class Header : Element, ISpecial
    {
    }

    public class Hgroup : Element, ISpecial
    {
    }

    public class Hr : Element, ISpecial
    {
    }

    public class Iframe : Element, ISpecial
    {
    }

    public class Img : Element, ISpecial
    {
    }

    public class Input : Element, ISpecial
    {
    }

    public class Isindex : Element, ISpecial
    {
    }

    public class Li : Element, ISpecial
    {
    }

    public class Link : Element, ISpecial
    {
    }

    public class Listing : Element, ISpecial
    {
    }

    public class Menu : Element, ISpecial
    {
    }

    public class Meta : Element, ISpecial
    {
#if false
        public Meta()
        {
        }

        public Meta(Token.StartTag t) :
            base(t)
        {
        }
#endif
    }

    public class Nav : Element, ISpecial
    {
    }

    public class Noembed : Element, ISpecial
    {
    }

    public class Noframes : Element, ISpecial
    {
    }

    public class Noscript : Element, ISpecial
    {
    }

    public class Ol : Element, ISpecial
    {
    }

    public class P : Element, ISpecial
    {
    }

    public class Param : Element, ISpecial
    {
    }

    public class Plaintext : Element, ISpecial
    {
    }

    public class Pre : Element, ISpecial
    {
    }

    public class Script : Element, ISpecial
    {
    }

    public class Section : Element, ISpecial
    {
    }

    public class Select : Element, ISpecial
    {
    }

    public class Spacer : Element, ISpecial
    {
    }

    public class Style : Element, ISpecial
    {
    }

    public class TableBody : Element, ISpecial
    {
    }

    public class Textarea : Element, ISpecial
    {
    }

    public class Tfoot : Element, ISpecial
    {
    }

    public class Thead : Element, ISpecial
    {
    }

    public class Title : Element, ISpecial
    {
    }

    public class TableRow : Element, ISpecial
    {
    }

    public class Ul : Element, ISpecial
    {
    }

    public class Wbr : Element, ISpecial
    {
    }

    public interface IScoping
    {
    }

    public class Applet : Element, IScoping
    {
    }

    public class Button : Element, IScoping
    {
    }

    public class TableCaption : Element, IScoping
    {
    }

    public class Html : Element, IScoping
    {
#if false
        public Html()
        {
        }

        public Html(Token.StartTag tag)
            : base(tag == null ? null : tag.Attributes)
        {
        }
#endif
    }

    public class Marquee : Element, IScoping
    {
    }

    public class Object : Element, IScoping
    {
    }

    public class Table : Element, IScoping
    {
    }

    public class TableCell : Element, IScoping
    {
    }

    public class Th : Element, IScoping
    {
    }

    public interface IFormatting
    {
    }

    public class Anchor : Element, IFormatting
    {
    }

    public class B : Element, IFormatting
    {
    }

    public class Big : Element, IFormatting
    {
    }

    public class Code : Element, IFormatting
    {
    }

    public class Em : Element, IFormatting
    {
    }

    public class Font : Element, IFormatting
    {
    }

    public class I : Element, IFormatting
    {
    }

    public class Nobr : Element, IFormatting
    {
    }

    public class S : Element, IFormatting
    {
    }

    public class Small : Element, IFormatting
    {
    }

    public class Strike : Element, IFormatting
    {
    }

    public class Strong : Element, IFormatting
    {
    }

    public class Tt : Element, IFormatting
    {
    }

    public class U : Element, IFormatting
    {
    }

    public interface IPhrasing
    {
    }

    public class Optgroup : Element, IPhrasing
    {
    }

    public class Option : Element, IPhrasing
    {
    }

    public class Rp : Element, IPhrasing
    {
    }

    public class Rt : Element, IPhrasing
    {
    }
}