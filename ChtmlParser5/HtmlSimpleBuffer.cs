﻿using System.IO;

namespace HtmlParser5
{
    /// <summary>
    /// Non-refilling buffer with simple methods.
    /// </summary>
    public sealed /*internal*/ class HtmlSimpleBuffer
    {
        /// <summary>
        /// End of stream.
        /// </summary>
        public const int EOF = -1;

        /// <summary>
        /// Buffer size.
        /// </summary>
        public const int BufferSize = 512;

        internal Stream Stream;

        /// <summary>
        /// Constructor from a Stream.
        /// </summary>
        /// <param name="s"></param>
        public HtmlSimpleBuffer(Stream s)
        {
            Stream = s;
        }

        /// <summary>
        /// Constructor from a buffer.
        /// </summary>
        /// <param name="buf"></param>
        /// <param name="len"></param>
        public HtmlSimpleBuffer(byte[] buf, int len)
        {
            Stream = null;
            Buffer = buf;
            Length = len;
        }

        /// <summary>
        /// Buffer.
        /// </summary>
        internal byte[] Buffer { get; set; }

        /// <summary>
        /// Buffer size.
        /// </summary>
        public int Length { get; private set; }

        /// <summary>
        /// Returns the byte specified or 0 if out of range.
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public int this[int i]
        {
            get
            {
                if (i >= Length || i < 0) return EOF;
                return Buffer[i];
            }
        }

        /// <summary>
        /// Returns the byte specified or 0 if out of range. Lowercase ASCII uppercase.
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public int GetLower(int i)
        {
            if (i >= Length || i < 0) return EOF;
            int c = Buffer[i];
            if (c >= 'A' && c <= 'Z') return c + 0x20;
            return c;
        }

        /// <summary>
        /// Fill the buffer. No-op if filled already.
        /// </summary>
        public void Fill()
        {
            if (Buffer != null)
                return;
            if (Stream == null)
            {
                Length = 0;
                return;
            }
            Buffer = new byte[BufferSize];
            Length = Stream.Read(Buffer, 0, Buffer.Length);
            if (Length == 0)
                Buffer = null;
        }
    }
}