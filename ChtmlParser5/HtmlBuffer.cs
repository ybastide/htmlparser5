﻿using System;
using System.IO;
using System.Text;

namespace HtmlParser5
{
    /// <summary>
    /// Buffer with peek and put-back.
    /// </summary>
    public sealed class HtmlBuffer
    {
        /// <summary>
        /// End of stream.
        /// </summary>
        public const int EOF = -1;

        /// <summary>
        /// Buffer size.
        /// </summary>
        public const int BufferSize = 512;

        private static readonly int[] Cp1252ToUnicode = new[]
                                                              {
                                                                  0x20ac, 0xfffd, 0x201a, 0x192, 0x201e, 0x2026, 0x2020,
                                                                  0x2021,
                                                                  0x2c6, 0x2030, 0x160, 0x2039, 0x152, 0xfffd, 0x17d,
                                                                  0xfffd,
                                                                  0xfffd, 0x2018, 0x2019, 0x201c, 0x201d, 0x2022, 0x2013
                                                                  , 0x2014,
                                                                  0x2dc, 0x2122, 0x161, 0x203a, 0x153, 0xfffd, 0x17e,
                                                                  0x178
                                                              };

        /// <summary>
        /// Byte buffer.
        /// </summary>
        private readonly byte[] _buffer;

        /// <summary>
        /// Encoding.
        /// </summary>
        private readonly Encoding _enc;

        /// <summary>
        /// Reading stream.
        /// </summary>
        private readonly Stream _stream;

        /// <summary>
        /// Byte length of the last decoded char.
        /// </summary>
        private int _currentCharLen;

        /// <summary>
        /// Decoded string for unsupported multibyte encodings.
        /// </summary>
        private string _decodedBuffer;

        /// <summary>
        /// Current position in the decoded string.
        /// </summary>
        private int _decodedBufferPosition;

        /// <summary>
        /// Buffer length.
        /// </summary>
        private int _length;

        /// <summary>
        /// Current position.
        /// </summary>
        private int _position;

        /// <summary>
        /// Pushed-back character (decoded).
        /// </summary>
        private int _pushedChar = EOF;

        private bool _started;
        private bool _littleEndian = true;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="simpleBuffer"></param>
        /// <param name="encoding"></param>
        public HtmlBuffer(HtmlSimpleBuffer simpleBuffer, Encoding encoding)
        {
            _stream = simpleBuffer.Stream;
            _buffer = simpleBuffer.Buffer ?? new byte[BufferSize];
            _length = simpleBuffer.Length;
            _enc = encoding;
            _position = 0;
        }

        /// <summary>
        /// Decode only utf-8, despite its name.
        /// </summary>
        /// <returns></returns>
        private int DecodeMultiByte()
        {
            // XXX LE/BE semble mal détecté (dû à des valeurs invalides)
            if (_enc.CodePage == 1200 || _enc.CodePage == 1201)
            {
                int ch = _littleEndian ? _buffer[_position] + _buffer[_position + 1] * 256 : _buffer[_position] * 256 + _buffer[_position + 1];
                _currentCharLen = 2;
                if (!_started)
                {
                    _started = true;
                    if (_length >= 2)
                    {
                        if (ch == 0xfeff)
                        {
                            //_littleEndian = false;
                        }
                        if ((ch == 0xfffe || ch == 0xfeff) && _length >= 4)
                        {
                            _currentCharLen = 4;
                            ch = _littleEndian ? _buffer[_position + 2] + _buffer[_position + 3] * 256 : _buffer[_position + 2] * 256 + _buffer[_position + 3];
                        }
                    }
                }
                if (ch >= 0xd800 && ch <= 0xdcff)
                {
                    if (_position + _currentCharLen + 2 > _length)
                        return ch;
                    int ch2 = _littleEndian ? _buffer[_position + _currentCharLen] + _buffer[_position + _currentCharLen + 1] * 256 : _buffer[_position + _currentCharLen] * 256 + _buffer[_position + _currentCharLen + 1];
                    if (ch2 < 0xdc00 || ch2 > 0xdfff)
                        return ch;
                    _currentCharLen += 2;
                    ch = 0x10000 + (((ch - 0xd800) << 10) + (ch2 - 0xdc00));
                }
                return ch;
            }
            if (_enc.CodePage == 12000/* || _enc.CodePage == 12001*/)
            {
                _currentCharLen = 4;
                int ch = _buffer[_position] + _buffer[_position + 1] * 256
                    + _buffer[_position + 2] * 65536;// +_buffer[_position + 3] * 256;
                return ch;
            }

            if (_enc.CodePage != 65000 && _enc.CodePage != 65001)
                throw new Exception("Unknown code page"); // can't happen (use decoded_buffer)
            _currentCharLen = 1;
            byte c = _buffer[_position];
            // ASCII
            if (c < 128)
                return c;
            // CP1252 mixed in the page...
            if (c < 160)
                return Cp1252ToUnicode[c - 128];
            if (c >= 0xC2 && c <= 0xDF)
                return DecodeUtf8(c, 0x1f, 1);
            if (c >= 0xE0 && c <= 0xEF)
                return DecodeUtf8(c, 0x0f, 2);
            if (c >= 0xF0 && c <= 0xF4)
                return DecodeUtf8(c, 0x07, 3);
            // Invalid encoding, treat as latin 1
            return c;
        }

        private int DecodeUtf8(byte c, int mask, int nBytesLeft)
        {
            int res = c & mask;
            for (int i = 0; i < nBytesLeft; i++)
            {
                int c2 = GetNextChar();
                if (c2 == EOF)
                    return c;
                if (!(c2 >= 0x80 && c2 <= 0xBF))
                {
                    _position -= i;
                    _currentCharLen -= i;
                    if (_position < 0)
                    {
                        // Error crossing the buffer size, we lose the start
                        _position = 0;
                        _currentCharLen = 0;
                    }
                    // treat as latin1.
                    return c;
                }
                res = (res << 6) | (c2 & 0x3f);
            }
            return res;
        }

        /// <summary>
        /// Get next character, refilling if needed, updating <see cref="_currentCharLen"/>.
        /// </summary>
        /// <returns></returns>
        private int GetNextChar()
        {
            if (_position + _currentCharLen == _length)
            {
                Fill();
                _currentCharLen = 0;
                if (_length == 0)
                    return EOF;
            }
            byte c = _buffer[_position + _currentCharLen];
            _currentCharLen++;
            return c;
        }

        /// <summary>
        /// Decode a character in a single-byte encoding.
        /// </summary>
        /// <returns></returns>
        private int DecodeSingleByte()
        {
            byte b = _buffer[_position];
            _currentCharLen = 1;
            switch (_enc.CodePage)
            {
                // Fast path
                case 20127: // ascii
                case 28591: // latin1
                case 1252:
                    if (b < 128 || b >= 160)
                        return b;
                    return Cp1252ToUnicode[b - 128];
                case 28605: // latin9
                    switch (b)
                    {
                        case 0xA4:
                            return '€';
                        case 0xA6:
                            return 'Š';
                        case 0xA8:
                            return 'š';
                        case 0xB4:
                            return 'Ž';
                        case 0xB8:
                            return 'ž';
                        case 0xBC:
                            return 'Œ';
                        case 0xBD:
                            return 'œ';
                        case 0xBE:
                            return 'Ÿ';
                    }
                    goto case 1252;
            }
            var ba = new[] { b };
            var c = new char[1];
            try
            {
                _enc.GetChars(ba, 0, 1, c, 0);
                return c[0];
            }
            catch
            {
                return 0xfffd;
            }
        }

        /// <summary>
        /// Fill the buffer.
        /// </summary>
        private void Fill()
        {
            _position = 0;
            if (_stream == null)
            {
                _length = 0;
                return;
            }
            _length = _stream.Read(_buffer, 0, _buffer.Length);
            if (!_enc.IsSingleByte && _enc.CodePage != 65000)
            {
                _decodedBuffer = _enc.GetString(_buffer, 0, _length);
                _decodedBufferPosition = 0;
            }
        }

        /// <summary>
        /// Peek at the next character.
        /// </summary>
        /// <returns></returns>
        public int Peek()
        {
            // Pushed char?
            if (_pushedChar != EOF)
            {
                int c = _pushedChar;
                return c;
            }
            // Unsupported encoding?
            if (_decodedBuffer != null)
            {
                if (_decodedBufferPosition == _decodedBuffer.Length)
                    Fill();
                if (_length != 0)
                    return _decodedBuffer[_decodedBufferPosition];
                return EOF;
            }
            // Normal case
            if (_position == _length)
                Fill();
            if (_length != 0)
            {
                if (_enc.IsSingleByte)
                    return DecodeSingleByte();
                return DecodeMultiByte();
            }
            return EOF;
        }

        /// <summary>
        /// Get the next character.
        /// </summary>
        /// <returns></returns>
        public int Get()
        {
            int c = Peek();
            if (_pushedChar != EOF)
                _pushedChar = EOF;
            else if (_decodedBuffer == null)
                _position += _currentCharLen;
            else
                _decodedBufferPosition++;
            return c;
        }

        public void Put(char c)
        {
            if (_pushedChar != EOF)
                throw new Exception("Character already put.");
            _pushedChar = c;
        }
    }
}