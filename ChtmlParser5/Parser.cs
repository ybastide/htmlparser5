﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using HtmlParser5.Dom;
using HtmlParser5.Html;
using Object = HtmlParser5.Html.Object;

namespace HtmlParser5
{
    public class Parser
    {
        private static readonly Dictionary<string, Element> TagNameToElement;
        private ParserState _parserState;
        private Document _doc;

        /// <summary>
        /// If true, will skip next LF.
        /// </summary>
        private bool _skipNextLf;

        private Tokenizer _tokenizer;

        static Parser()
        {
            TagNameToElement = new Dictionary<string, Element>();
            TagNameToElement["address"] = new Address();
            TagNameToElement["area"] = new Area();
            TagNameToElement["article"] = new Article();
            TagNameToElement["aside"] = new Aside();
            TagNameToElement["base"] = new Base();
            TagNameToElement["basefont"] = new Basefont();
            TagNameToElement["bgsound"] = new Bgsound();
            TagNameToElement["blockquote"] = new Blockquote();
            TagNameToElement["body"] = new Body();
            TagNameToElement["br"] = new Br();
            TagNameToElement["center"] = new Center();
            TagNameToElement["col"] = new Col();
            TagNameToElement["colgroup"] = new Colgroup();
            TagNameToElement["command"] = new Command();
            TagNameToElement["datagrid"] = new Datagrid();
            TagNameToElement["dd"] = new Dd();
            TagNameToElement["details"] = new Details();
            TagNameToElement["dialog"] = new Dialog();
            TagNameToElement["dir"] = new Dir();
            TagNameToElement["div"] = new Div();
            TagNameToElement["dl"] = new Dl();
            TagNameToElement["dt"] = new Dt();
            TagNameToElement["embed"] = new Embed();
            TagNameToElement["fieldset"] = new Fieldset();
            TagNameToElement["figure"] = new Figure();
            TagNameToElement["footer"] = new Footer();
            TagNameToElement["form"] = new Form();
            TagNameToElement["frame"] = new Frame();
            TagNameToElement["frameset"] = new Frameset();
            TagNameToElement["h1"] = new H1();
            TagNameToElement["h2"] = new H2();
            TagNameToElement["h3"] = new H3();
            TagNameToElement["h4"] = new H4();
            TagNameToElement["h5"] = new H5();
            TagNameToElement["h6"] = new H6();
            TagNameToElement["head"] = new Head();
            TagNameToElement["header"] = new Header();
            TagNameToElement["hgroup"] = new Hgroup();
            TagNameToElement["hr"] = new Hr();
            TagNameToElement["iframe"] = new Iframe();
            TagNameToElement["img"] = new Img();
            TagNameToElement["input"] = new Input();
            TagNameToElement["isindex"] = new Isindex();
            TagNameToElement["li"] = new Li();
            TagNameToElement["link"] = new Link();
            TagNameToElement["listing"] = new Listing();
            TagNameToElement["menu"] = new Menu();
            TagNameToElement["meta"] = new Meta();
            TagNameToElement["nav"] = new Nav();
            TagNameToElement["noembed"] = new Noembed();
            TagNameToElement["noframes"] = new Noframes();
            TagNameToElement["noscript"] = new Noscript();
            TagNameToElement["ol"] = new Ol();
            TagNameToElement["p"] = new P();
            TagNameToElement["param"] = new Param();
            TagNameToElement["plaintext"] = new Plaintext();
            TagNameToElement["pre"] = new Pre();
            TagNameToElement["script"] = new Script();
            TagNameToElement["section"] = new Section();
            TagNameToElement["select"] = new Select();
            TagNameToElement["spacer"] = new Spacer();
            TagNameToElement["style"] = new Style();
            TagNameToElement["tbody"] = new TableBody();
            TagNameToElement["textarea"] = new Textarea();
            TagNameToElement["tfoot"] = new Tfoot();
            TagNameToElement["thead"] = new Thead();
            TagNameToElement["title"] = new Title();
            TagNameToElement["tr"] = new TableRow();
            TagNameToElement["ul"] = new Ul();
            TagNameToElement["wbr"] = new Wbr();
            TagNameToElement["applet"] = new Applet();
            TagNameToElement["button"] = new Button();
            TagNameToElement["caption"] = new TableCaption();
            TagNameToElement["html"] = new Html.Html();
            TagNameToElement["marquee"] = new Marquee();
            TagNameToElement["object"] = new Object();
            TagNameToElement["table"] = new Table();
            TagNameToElement["td"] = new TableCell();
            TagNameToElement["th"] = new Th();
            TagNameToElement["a"] = new Anchor();
            TagNameToElement["b"] = new B();
            TagNameToElement["big"] = new Big();
            TagNameToElement["code"] = new Code();
            TagNameToElement["em"] = new Em();
            TagNameToElement["font"] = new Font();
            TagNameToElement["i"] = new I();
            TagNameToElement["nobr"] = new Nobr();
            TagNameToElement["s"] = new S();
            TagNameToElement["small"] = new Small();
            TagNameToElement["strike"] = new Strike();
            TagNameToElement["strong"] = new Strong();
            TagNameToElement["tt"] = new Tt();
            TagNameToElement["u"] = new U();
        }

        private bool ParseError
        {
            get { return _parserState.Error; }
            set { _parserState.Error = value; }
        }

        private Node CurrentNode
        {
            get { return _parserState.CurrentNode; }
            set { _parserState.OpenElements.Push(value); }
        }


        public void Parse(Stream stream, Encoding encoding)
        {
            _parserState = new ParserState();
            _tokenizer = new Tokenizer(stream, encoding, _parserState);
            _doc = new Document();
            DoParse();
        }

        private static bool IsSpace(Token t)
        {
            var tc = t as Token.Character;
            if (tc != null)
            {
                if (tc.Ch == ' ' || tc.Ch == '\n' || tc.Ch == '\f' || tc.Ch == '\t')
                    return true;
            }
            return false;
        }

        private static bool IsStartTag(Token t, string tagName)
        {
            return t is Token.StartTag && ((Token.StartTag)t).TagName == tagName;
        }

        private static bool IsEndTag(Token t, string tagName)
        {
            return t is Token.EndTag && ((Token.EndTag)t).TagName == tagName;
        }

        private static bool IsStartTag(Token t, params string[] tagNames)
        {
            if (!(t is Token.StartTag))
                return false;
            if (tagNames.Length == 0) return true;
            string name = ((Token.StartTag)t).TagName;
            foreach (string tagName in tagNames)
                if (name == tagName)
                    return true;
            return false;
        }

        private static bool IsEndTag(Token t, params string[] tagNames)
        {
            if (!(t is Token.EndTag))
                return false;
            if (tagNames.Length == 0) return true;
            string name = ((Token.EndTag)t).TagName;
            foreach (string tagName in tagNames)
                if (name == tagName)
                    return true;
            return false;
        }

        //private static bool IsStartTag(Token t)
        //{
        //    return t is Token.StartTag;
        //}

        //private static bool IsEndTag(Token t)
        //{
        //    return t is Token.EndTag;
        //}

        /*
                private static bool IsStartTag(Token.StartTag t, string tagName)
                {
                    return t.TagName == tagName;
                }
        */

        /*
                private static bool IsEndTag(Token.EndTag t, string tagName)
                {
                    return t.TagName == tagName;
                }
        */

        private static bool IsStartTag(Token.StartTag t, params string[] tagNames)
        {
            string name = t.TagName;
            foreach (string tagName in tagNames)
                if (name == tagName)
                    return true;
            return false;
        }

        private static bool IsEndTag(Token.EndTag t, params string[] tagNames)
        {
            string name = t.TagName;
            foreach (string tagName in tagNames)
                if (name == tagName)
                    return true;
            return false;
        }

        private void DoParse()
        {
            foreach (Token t in _tokenizer)
            {
                if (_skipNextLf)
                {
                    _skipNextLf = false;
                    if (t is Token.Character && ((Token.Character)t).Ch == '\n')
                        continue;
                }
                // End tag with attributes?
                if (t is Token.EndTag && ((Token.EndTag)t).HasAttributes)
                    ParseError = true;
                switch (_parserState.InsertionMode)
                {
                    case InsertionMode.Initial:
                        ParseInitial(t);
                        break;
                    case InsertionMode.BeforeHtml:
                        ParseBeforeHtml(t);
                        break;
                    case InsertionMode.BeforeHead:
                        ParseBeforeHead(t);
                        break;
                    case InsertionMode.InHead:
                        ParseInHead(t);
                        break;
                    case InsertionMode.InHeadNoScript:
                        ParseInHeadNoScript(t);
                        break;
                    case InsertionMode.AfterHead:
                        ParseAfterHead(t);
                        break;
                    case InsertionMode.InBody:
                        ParseInBody(t);
                        break;
                    case InsertionMode.AfterBody:
                        ParseAfterBody(t);
                        break;
                    default:
                        throw new Exception("235");
                }
                // Self-closing flag not acknowledge.
                if (t is Token.Tag && ((Token.Tag)t).SelfClosing)
                    ParseError = true;
                if (t is Token.EOF)
                    break;
            }
        }

        private void ParseInHeadNoScript(Token t)
        {
            if (t is Token.DocType)
            {
                ParseError = true;
            }
            else if (IsStartTag(t, "html"))
            {
                ParseInBody(t);
            }
            else if (IsEndTag(t, "noscript"))
            {
                ParseNoscript();
            }
            else if (IsSpace(t) || t is Token.Comment
                     || IsStartTag(t, "link", "meta", "noframes", "style"))
            {
                ParseInHead(t);
            }
            else if ((IsEndTag(t) && !IsEndTag(t, "br")) || IsStartTag(t, "head", "nothing"))
            {
                ParseError = true;
            }
            else
            {
                ParseError = true;
                ParseNoscript();
                ParseInHead(t);
            }
        }

        private void ParseNoscript()
        {
            Node pop = _parserState.OpenElements.Pop();
            if (!(pop is Noscript))
                throw new Exception("257");
            if (!(CurrentNode is Head))
                throw new Exception("259");
            _parserState.InsertionMode = InsertionMode.InHead;
        }

        private void ParseBeforeHtml(Token t)
        {
            if (IsSpace(t))
                return;
            if (t is Token.DocType)
            {
                ParseError = true;
            }
            else if (t is Token.Comment)
            {
                var tc = (Token.Comment)t;
                _doc.AppendChild(new Comment(tc));
            }
            else if (IsStartTag(t, "html"))
            {
                Node n = new Html.Html().SetAttributes((Token.StartTag)t);
                _doc.AppendChild(n);
                CurrentNode = n;
                _parserState.InsertionMode = InsertionMode.BeforeHead;
            }
            else
            {
                Node n = new Html.Html();
                _doc.AppendChild(n);
                CurrentNode = n;
                _parserState.InsertionMode = InsertionMode.BeforeHead;
            }
        }

        private void ParseBeforeHead(Token t)
        {
            if (IsSpace(t))
                return;
            if (t is Token.Comment)
            {
                var tc = (Token.Comment)t;
                CurrentNode.AppendChild(new Comment(tc));
            }
            else if (t is Token.DocType)
            {
                ParseError = true;
            }
            else if (IsStartTag(t, "html"))
            {
                ParseInBody(t);
            }
            else if (IsStartTag(t, "head"))
            {
                Node n = new Head().SetAttributes((Token.StartTag)t);
                InsertHtmlElement(n);
                _parserState.Head = (Head)n;
                _parserState.InsertionMode = InsertionMode.InHead;
            }
            else if (IsEndTag(t, "head", "body", "html", "br"))
            {
                ParseBeforeHead(new Token.StartTag("head"));
                ParseInHead(t);
            }
            else if (t is Token.EndTag)
            {
                ParseError = true;
            }
            else
            {
                ParseBeforeHead(new Token.StartTag("head"));
                ParseInHead(t);
            }
        }

        private void ParseInHead(Token t)
        {
            if (IsSpace(t))
            {
                InsertCharacter((Token.Character)t);
                return;
            }
            if (t is Token.Comment)
            {
                var tc = (Token.Comment)t;
                CurrentNode.AppendChild(new Comment(tc));
                return;
            }
            if (t is Token.DocType)
            {
                ParseError = true;
                return;
            }
            if (IsStartTag(t))
            {
                var tc = (Token.StartTag)t;
                if (tc.TagName == "html")
                {
                    ParseInBody(t);
                    return;
                }
                if (IsStartTag(tc, "base", "command", "link"))
                {
                    AppendElementByTag(tc);
                    AckSelfClosingFlag(tc);
                    return;
                }
                if (tc.TagName == "meta")
                {
                    Element e = new Meta().SetAttributes(tc);
                    CurrentNode.AppendChild(e);
                    AckSelfClosingFlag(tc);
#if false
    // currently unsupported (only content-type & sniffing)
                    string s;
                    if (e.Attributes.TryGetValue("charset", out s))
                    {

                    }
                    if (e.Attributes.TryGetValue("encoding", out s))
                    {

                    }
#endif
                    return;
                }
                if (tc.TagName == "title")
                {
                    GenericRcDataOrRawTextParsing(tc, false);
                    return;
                }
                if (tc.TagName == "noframes" || tc.TagName == "style")
                {
                    GenericRcDataOrRawTextParsing(tc, true);
                    return;
                }
                if (tc.TagName == "noscript")
                {
                    InsertHtmlElement(tc);
                    _parserState.InsertionMode = InsertionMode.InHeadNoScript;
                    return;
                }
                if (tc.TagName == "script")
                {
                    Element n = CreateElementForToken(tc);
                    n.ParserInserted = true;
                    InsertHtmlElement(n);
                    _tokenizer.ContentModel = ContentModel.RawText;
                    _parserState.OriginalInsertionMode = _parserState.InsertionMode;
                    _parserState.InsertionMode = InsertionMode.InRawTextRcData;
                    return;
                }
                if (tc.TagName == "head")
                {
                    ParseError = true;
                    return;
                }
            }
            else if (IsEndTag(t))
            {
                var tc = (Token.EndTag)t;
                if (tc.TagName == "head")
                {
                    Node n = _parserState.OpenElements.Pop();
                    if (!(n is Head))
                        throw new Exception("418");
                    _parserState.InsertionMode = InsertionMode.AfterHead;
                }
                else if (IsEndTag(tc, "body", "html", "br"))
                {
                    //redundant: goto anythingElse;
                }
                else
                {
                    ParseError = true;
                }
            }
            //anythingElse:
            Node pop = _parserState.OpenElements.Pop();
            if (!(pop is Head))
                throw new Exception("425");
            _parserState.InsertionMode = InsertionMode.AfterHead;
            ParseAfterHead(t);
        }

        private void ParseAfterHead(Token t)
        {
            if (IsSpace(t))
            {
                InsertCharacter((Token.Character)t);
                return;
            }
            if (t is Token.Comment)
            {
                var tc = (Token.Comment)t;
                CurrentNode.AppendChild(new Comment(tc));
                return;
            }
            if (t is Token.DocType)
            {
                ParseError = true;
                return;
            }
            if (IsStartTag(t))
            {
                var tc = (Token.StartTag)t;
                if (tc.TagName == "html")
                {
                    ParseInBody(t);
                    return;
                }
                if (tc.TagName == "body")
                {
                    InsertHtmlElement(tc);
                    _parserState.FramesetOk = false;
                    _parserState.InsertionMode = InsertionMode.InBody;
                    return;
                }
                if (tc.TagName == "frameset")
                {
                    InsertHtmlElement(tc);
                    _parserState.InsertionMode = InsertionMode.InFrameset;
                    return;
                }
                if (IsStartTag(tc, "base", "link", "meta", "noframes", "script", "style", "title"))
                {
                    ParseError = true;
                    CurrentNode = _parserState.Head;
                    ParseInHead(t);
                    _parserState.OpenElements.Pop();
                    return;
                }
                if (tc.TagName == "head")
                {
                    ParseError = true;
                    return;
                }
            }
            else if (IsEndTag(t) && !IsEndTag(t, "body", "html", "br"))
            {
                ParseError = true;
                return;
            }
            InsertHtmlElement(new Body());
            _parserState.InsertionMode = InsertionMode.InBody;
            ParseInBody(t);
        }

        private void ParseInBody(Token t)
        {
            var character = t as Token.Character;
            if (character != null)
            {
                _parserState.ReconstructActiveFormattingElements();
                InsertCharacter(character);
                if (!IsSpace(t))
                    _parserState.FramesetOk = false;
                return;
            }
            var tc = t as Token.Comment;
            if (tc != null)
            {
                CurrentNode.AppendChild(new Comment(tc));
                return;
            }
            if (t is Token.DocType)
            {
                ParseError = true;
                return;
            }
            if (IsStartTag(t))
            {
                var ts = (Token.StartTag)t;
                if (ts.TagName == "html")
                {
                    ParseError = true;
                    Element html = _parserState.Html;
                    foreach (Token.Tag.Attribute attr in ts.Attributes)
                    {
                        if (!html.Attributes.ContainsKey(attr.Name))
                            html.Attributes[attr.Name] = attr.Value;
                    }
                    return;
                }
                if (IsStartTag(ts, "base", "command", "link", "meta", "noframes", "script", "style", "title"))
                {
                    ParseInHead(t);
                    return;
                }
                if (ts.TagName == "body")
                {
                    ParseError = true;
                    Body body = _parserState.GetBody();
                    if (body != null)
                    {
                        foreach (Token.Tag.Attribute attr in ts.Attributes)
                        {
                            if (!body.Attributes.ContainsKey(attr.Name))
                                body.Attributes[attr.Name] = attr.Value;
                        }
                        return;
                    }
                }
                if (ts.TagName == "frameset")
                {
                    ParseError = true;
                    Body body = _parserState.GetBody();
                    if (!_parserState.FramesetOk || body == null)
                        return;
                    Node n = body.ParentNode;
                    if (n != null)
                        n.RemoveChild(body);
                    do
                    {
                        n = _parserState.OpenElements.Pop();
                    } while (!(n is Html.Html));
                    _parserState.OpenElements.Push(n);
                    InsertHtmlElement(ts);
                    _parserState.InsertionMode = InsertionMode.InFrameset;
                    return;
                }
                if (IsStartTag(ts, "address", "article", "aside", "blockquote", "center",
                               "datagrid", "details", "dialog", "dir", "div", "dl", "fieldset", "figure",
                               "footer", "header", "hgroup", "menu", "nav", "ol", "p", "section", "ul"))
                {
                    EndParaIfNeeded();
                    InsertHtmlElement(ts);
                    return;
                }
                if (IsStartTag(ts, "h1", "h2", "h3", "h4", "h5", "h6"))
                {
                    EndParaIfNeeded();
                    Node n = CurrentNode;
                    if (n != null && n is IH)
                    {
                        ParseError = true;
                        _parserState.OpenElements.Pop();
                    }
                    InsertHtmlElement(ts);
                    return;
                }
                if (IsStartTag(ts, "pre", "listing"))
                {
                    EndParaIfNeeded();
                    InsertHtmlElement(ts);
                    _parserState.FramesetOk = false;
                    _skipNextLf = true;
                    return;
                }
                if (ts.TagName == "form")
                {
                    if (_parserState.Form != null)
                    {
                        ParseError = true;
                        return;
                    }
                    EndParaIfNeeded();
                    Node n = new Form().SetAttributes(ts);
                    InsertHtmlElement(n);
                    _parserState.Form = (Form)n;
                    return;
                }
                if (ts.TagName == "li")
                {
                    _parserState.FramesetOk = false;
                    if (CurrentNode is Li)
                        _ParseEndLi();
                    else
                    {
                        while (true)
                        {
                            Node n = CurrentNode;
                            if (!(n is IFormatting || n is IPhrasing || n is Address || n is Div || n is Param))
                                break;
                            _parserState.OpenElements.Pop();
                        }
                    }
                    EndParaIfNeeded();
                    InsertHtmlElement(ts);
                    return;
                }
                if (ts.TagName == "dd" || ts.TagName == "dt")
                {
                    _parserState.FramesetOk = false;
                    while (true)
                    {
                        Node n = CurrentNode;
                        if (n is Dd || n is Dt)
                        {
                            _ParseEndDdDt(n);
                            break;
                        }
                        if (!(n is IFormatting || n is IPhrasing || n is Address || n is Div || n is Param))
                            break;
                        _parserState.OpenElements.Pop();
                    }
                    EndParaIfNeeded();
                    InsertHtmlElement(ts);
                    return;
                }
                if (ts.TagName == "plaintext")
                {
                    EndParaIfNeeded();
                    InsertHtmlElement(ts);
                    _tokenizer.ContentModel = ContentModel.PlainText;
                    return;
                }
                if (ts.TagName == "a")
                {
                    Node n = CreateElementForToken(ts);
                    if (_parserState.IsInActiveFormattingElements(n))
                    {
                        ParseError = true;
                        ParseInBody(new Token.EndTag("a"));
                        _parserState.RemoveActiveFormattingElement(n);
                        // XXX to check
                        while (CurrentNode is Anchor)
                            _parserState.OpenElements.Pop();
                    }
                    _parserState.ReconstructActiveFormattingElements();
                    InsertHtmlElement(n);
                    _parserState.ActiveFormattingElements.Add(n);
                    return;
                }
                if (IsStartTag(ts, "b", "big", "code", "em", "font", "i", "s", "small", "strike", "strong", "tt", "u"))
                {
                    _parserState.ReconstructActiveFormattingElements();
                    InsertHtmlElement(ts);
                    _parserState.ActiveFormattingElements.Add(CurrentNode);
                    return;
                }
                if (ts.TagName == "nobr")
                {
                    _parserState.ReconstructActiveFormattingElements();
                    Node n = CreateElementForToken(ts);
                    if (_parserState.IsNodeInScope(n))
                    {
                        ParseError = true;
                        ParseInBody(new Token.EndTag("nobr"));
                        _parserState.ReconstructActiveFormattingElements();
                    }
                    InsertHtmlElement(n);
                    _parserState.ActiveFormattingElements.Add(n);
                    return;
                }
            }
            if (t is Token.EOF)
            {
                foreach (Node n in _parserState.OpenElements)
                    if (!(n is Dd || n is Dt || n is Li || n is P
                          || n is TableBody || n is TableCell || n is Tfoot
                          || n is Th || n is Thead || n is TableRow
                          || n is Body || n is Html.Html))
                    {
                        ParseError = true;
                        break;
                    }
                return;
            }
            if (IsEndTag(t))
            {
                var te = (Token.EndTag)t;
                if (te.TagName == "body")
                {
                    ParseEndBody();
                    return;
                }
                if (te.TagName == "html")
                {
                    if (ParseEndBody())
                    {
                        ParseAfterBody(t);
                    }
                    return;
                }
                if (IsEndTag(te, "address", "article", "aside", "blockquote", "center", "datagrid",
                             "details", "dialog", "dir", "div", "dl", "fieldset", "figure", "footer",
                             "header", "hgroup", "listing", "menu", "nav", "ol", "pre", "section", "ul"))
                {
                    Node n = TagNameToElement[te.TagName];
                    if (!_parserState.IsNodeInScope(n))
                    {
                        ParseError = true;
                    }
                    else
                    {
                        GenerateImpliedEndTags();
                        if (CurrentNode.GetType() != n.GetType())
                            ParseError = true;
                        Node pop;
                        do
                        {
                            pop = _parserState.OpenElements.Pop();
                        } while (pop.GetType() != n.GetType());
                    }
                    return;
                }
                if (te.TagName == "form")
                {
                    Node n = _parserState.Form;
                    _parserState.Form = null;
                    if (n == null || !_parserState.IsNodeInScope(n))
                    {
                        ParseError = true;
                    }
                    else
                    {
                        GenerateImpliedEndTags();
                        if (CurrentNode != n)
                            ParseError = true;
                        else
                            _parserState.OpenElements.Pop();
                    }
                    return;
                }
                if (te.TagName == "p")
                {
                    if (!_parserState.IsNodeInScope(TagNameToElement["p"]))
                    {
                        ParseError = true;
                        //can't happen... EndParaIfNeeded();
                        InsertHtmlElement(new P());
                    }
                    GenerateImpliedEndTagsExcept(TagNameToElement["p"]);
                    if (!(CurrentNode is P))
                        ParseError = true;
                    Node pop;
                    do
                    {
                        pop = _parserState.OpenElements.Pop();
                    } while (!(pop is P));
                    return;
                }
                if (IsEndTag(te, "dd", "dt", "li"))
                {
                    Element n = TagNameToElement[te.TagName];
                    if (!_parserState.IsNodeInScope(n))
                    {
                        ParseError = true;
                    }
                    else
                    {
                        GenerateImpliedEndTagsExcept(n);
                        if (CurrentNode.GetType() != n.GetType())
                            ParseError = true;
                        Node pop;
                        do
                        {
                            pop = _parserState.OpenElements.Pop();
                        } while (pop.GetType() != n.GetType());
                    }
                    return;
                }
                if (IsEndTag(te, "h1", "h2", "h3", "h4", "h5", "h6"))
                {
                    Element n = TagNameToElement[te.TagName];
                    if (!_parserState.IsNodeInScope(n))
                    {
                        ParseError = true;
                    }
                    else
                    {
                        GenerateImpliedEndTags();
                        if (CurrentNode.GetType() != n.GetType())
                            ParseError = true;
                        Node pop;
                        do
                        {
                            pop = _parserState.OpenElements.Pop();
                        } while (pop.GetType() != n.GetType());
                    }
                    return;
                }
                if (IsEndTag(te, "a", "b", "big", "code", "em", "font", "i", "nobr", "s", "small",
                             "strike", "strong", "tt", "u"))
                {
                    throw new NotImplementedException();
                    return;
                }
            }
        }

        private void _ParseEndDdDt(Node node)
        {
            throw new NotImplementedException();
        }

        private void _ParseEndLi()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Pop open elements with implied tags, except for the one given.
        /// </summary>
        /// <param name="element"></param>
        private void GenerateImpliedEndTagsExcept(Element element)
        {
            while (true)
            {
                Node n = CurrentNode;
                if (n.GetType() != element.GetType())
                {
                    if (!(n is Dd || n is Dt || n is Li || n is Option
                          || n is Optgroup || n is P || n is Rp || n is Rt))
                        break;
                }
                _parserState.OpenElements.Pop();
            }
        }

        /// <summary>
        /// Pop open elements with implied tags.
        /// </summary>
        private void GenerateImpliedEndTags()
        {
            while (true)
            {
                Node n = CurrentNode;
                if (!(n is Dd || n is Dt || n is Li || n is Option
                      || n is Optgroup || n is P || n is Rp || n is Rt))
                    break;
                _parserState.OpenElements.Pop();
            }
        }

        /// <summary>
        /// End a potential &lt;/p>.
        /// </summary>
        private void EndParaIfNeeded()
        {
            if (_parserState.IsNodeInScope(TagNameToElement["p"]))
            {
                _ParseBodyEndPara();
            }
        }

        /// <summary>
        /// &lt;/p> in body.
        /// </summary>
        private void _ParseBodyEndPara()
        {
            throw new NotImplementedException();
        }

        private void ParseAfterBody(Token t)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Parse an end body tag in body mode.
        /// </summary>
        /// <returns>true if no error (body in scope).</returns>
        private bool ParseEndBody()
        {
            if (!_parserState.IsNodeInScope(TagNameToElement["body"]))
            {
                ParseError = true;
                return false;
            }

            foreach (Node n in _parserState.OpenElements)
                if (!(n is Dd || n is Dt || n is Li
                      || n is Optgroup || n is Option
                      || n is P
                      || n is Rp || n is Rt
                      || n is TableBody || n is TableCell || n is Tfoot
                      || n is Th || n is Thead || n is TableRow
                      || n is Body || n is Html.Html))
                {
                    ParseError = true;
                    break;
                }
            _parserState.InsertionMode = InsertionMode.AfterBody;
            return true;
        }

        /// <summary>
        /// Inserts a node, make in current.
        /// </summary>
        /// <param name="n"></param>
        private void InsertHtmlElement(Node n)
        {
            CurrentNode.AppendChild(n);
            CurrentNode = n;
        }

        /// <summary>
        /// Inserts a node, make it current.
        /// </summary>
        /// <param name="tc"></param>
        private void InsertHtmlElement(Token.StartTag tc)
        {
            InsertHtmlElement(CreateElementForToken(tc));
        }

        private void GenericRcDataOrRawTextParsing(Token.StartTag tc, bool raw)
        {
            InsertHtmlElement(CreateElementForToken(tc));
            _tokenizer.ContentModel = raw ? ContentModel.RawText : ContentModel.RcData;
            _parserState.OriginalInsertionMode = _parserState.InsertionMode;
            _parserState.InsertionMode = InsertionMode.InRawTextRcData;
        }

        /// <summary>
        /// Acknowledge (OK, reset) the token's self-closing flag, if it is set.
        /// </summary>
        /// <param name="tc"></param>
        private void AckSelfClosingFlag(Token.Tag tc)
        {
            // Just reset the flag.
            //if (tc.SelfClosing)
            tc.SelfClosing = false;
        }

        /// <summary>
        /// Create an element by tag.
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        private Element CreateElementForToken(Token.StartTag t)
        {
            Element node = TagNameToElement[t.TagName];
            node = (Element)node.CloneNode();
            node.SetAttributes(t.Attributes);
            return node;
        }

        /// <summary>
        /// Append an HTML element corresponding to a tag.
        /// </summary>
        /// <param name="t"></param>
        private void AppendElementByTag(Token.StartTag t)
        {
            Node type = CreateElementForToken(t);
            CurrentNode.AppendChild(type);
        }

        private void InsertCharacter(Token.Character t)
        {
            if (CurrentNode.LastChild != null && CurrentNode.LastChild is Text)
                ((Text)CurrentNode.LastChild).Append(t.Ch);
            else
                CurrentNode.AppendChild(new Text(t.Ch));
        }

        private void ParseInitial(Token t)
        {
            if (IsSpace(t))
                return;
            if (t is Token.Comment)
            {
                var tc = (Token.Comment)t;
                _doc.AppendChild(new Comment(tc));
            }
            else if (t is Token.DocType)
            {
                var dt = (Token.DocType)t;
                bool quirks = false;
                // XXX we don't set quirks mode correctly here.
                if (dt.Name.Equals("html", StringComparison.OrdinalIgnoreCase))
                {
                    string pub = dt.PublicIdentifier;
                    string sys = dt.SystemIdentifier;
                    if ((pub == "-//W3C//DTD HTML 4.0//EN"
                         && (sys == null || sys == "http://www.w3.org/TR/REC-html40/strict.dtd"))
                        || (pub == "-//W3C//DTD HTML 4.01//EN"
                            && (sys == null || sys == "http://www.w3.org/TR/html4/strict.dtd"))
                        || (pub == "-//W3C//DTD XHTML 1.0 Strict//EN"
                            && sys == "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd")
                        || (pub == "-//W3C//DTD XHTML 1.1//EN"
                            && sys == "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd")
                        )
                    {
                        // nothing
                    }
                    else
                    {
                        if (pub != null && sys != null && sys != "about:legacy-compat")
                            _parserState.Error = true;
                        quirks = true;
                    }
                }
                else
                {
                    quirks = true;
                }
                _doc.AppendChild(new DocumentType(dt));
                if (dt.ForceQuirks || quirks)
                    _doc.QuirksMode = true;

                _parserState.InsertionMode = InsertionMode.BeforeHtml;
            }
            else
            {
                _parserState.Error = true;
                _doc.QuirksMode = true;
                _parserState.InsertionMode = InsertionMode.BeforeHtml;
            }
        }
    }
}