﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace HtmlParser5
{
    /// <summary>
    /// Token.
    /// </summary>
    public class Token
    {
        private static readonly ParseError SParseError;

        public static ParseError ParseError { get { return SParseError; } }

        static Token()
        {
            SParseError = ParseError.GetSingleton();
        }

        #region Nested type: Character

        /// <summary>
        /// Character. 21 bits!.
        /// </summary>
        public class Character : Token
        {
            public char Ch;
            public char Surrogate;

            public Character(int c)
            {
                if (c >= 0x10000)
                {
                    c -= 0x10000;
                    int v1 = (c >> 10) | 0xD800;
                    int v2 = (c & 0x3ff) | 0xDC00;
                    Ch = (char)v1;
                    Surrogate = (char)v2;
                }
                else
                {
                    Ch = (char)c;
                }
            }

            public static string UnicodeToSurrogates(int c)
            {
                if (c < 0x10000)
                    return new string((char)c, 1);
                var cc = new char[2];
                c -= 0x10000;
                int v1 = (c >> 10) | 0xD800;
                int v2 = (c & 0x3ff) | 0xDC00;
                cc[0] = (char)v1;
                cc[1] = (char)v2;
                return new string(cc);
            }
        }

        #endregion

        #region Nested type: Comment

        /// <summary>
        /// Comment.
        /// </summary>
        public class Comment : Token
        {
            private string _content;
            private StringBuilder _contentBuilder;

            public Comment(StringBuilder sb)
            {
                _contentBuilder = sb;
            }

            public Comment()
            {
                _contentBuilder = new StringBuilder();
            }

            [DebuggerBrowsable(DebuggerBrowsableState.Never)]
            public string Content
            {
                get
                {
                    if (_content == null)
                    {
                        _content = _contentBuilder.ToString();
                        _contentBuilder = null;
                    }
                    return _content;
                }
            }

            //[Obsolete("", true)]
            //public void Append(char c)
            //{
            //}

            public void Append(int c)
            {
                _contentBuilder.Append(Character.UnicodeToSurrogates(c));
            }

            public void Append(string s)
            {
                _contentBuilder.Append(s);
            }
        }

        #endregion

        #region Nested type: DocType

        /// <summary>
        /// Document type.
        /// </summary>
        public class DocType : Token
        {
            public bool ForceQuirks;
            private string _name;
            private StringBuilder _nameBuilder = new StringBuilder();
            private string _publicIdentifier;
            private StringBuilder _publicIdentifierBuilder;
            private string _systemIdentifier;
            private StringBuilder _systemIdentifierBuilder;

            public DocType()
            {
            }

            public DocType(bool forceQuirks)
            {
                ForceQuirks = forceQuirks;
            }

            [DebuggerBrowsable(DebuggerBrowsableState.Never)]
            public string Name
            {
                get
                {
                    if (_name == null)
                    {
                        if (_nameBuilder != null)
                            _name = _nameBuilder.ToString();
                        _nameBuilder = null;
                    }
                    return _name;
                }
            }

            [DebuggerBrowsable(DebuggerBrowsableState.Never)]
            public string PublicIdentifier
            {
                get
                {
                    if (_publicIdentifier == null)
                    {
                        if (_publicIdentifierBuilder != null)
                            _publicIdentifier = _publicIdentifierBuilder.ToString();
                        _publicIdentifierBuilder = null;
                    }
                    return _publicIdentifier;
                }
            }

            [DebuggerBrowsable(DebuggerBrowsableState.Never)]
            public string SystemIdentifier
            {
                get
                {
                    if (_systemIdentifier == null)
                    {
                        if (_systemIdentifierBuilder != null)
                            _systemIdentifier = _systemIdentifierBuilder.ToString();
                        _systemIdentifierBuilder = null;
                    }
                    return _systemIdentifier;
                }
            }

            public void AppendToName(int c)
            {
                _nameBuilder.Append(Character.UnicodeToSurrogates(c));
            }

            // ReSharper disable InconsistentNaming
            public void AppendToPI(int c)
            // ReSharper restore InconsistentNaming
            {
                _publicIdentifierBuilder.Append(Character.UnicodeToSurrogates(c));
            }

            // ReSharper disable InconsistentNaming
            public void InitializePI()
            // ReSharper restore InconsistentNaming
            {
                _publicIdentifierBuilder = new StringBuilder();
            }

            // ReSharper disable InconsistentNaming
            public void AppendToSI(int c)
            // ReSharper restore InconsistentNaming
            {
                _systemIdentifierBuilder.Append(Character.UnicodeToSurrogates(c));
            }

            // ReSharper disable InconsistentNaming
            public void InitializeSI()
            // ReSharper restore InconsistentNaming
            {
                _systemIdentifierBuilder = new StringBuilder();
            }
        }

        #endregion

        #region Nested type: EOF

        /// <summary>
        /// End of stream.
        /// </summary>
        public class EOF : Token
        {
        }

        #endregion

        #region Nested type: EndTag

        /// <summary>
        /// End tag.
        /// </summary>
        public class EndTag : Tag
        {
            [Obsolete("", true)]
            public EndTag(char c)
                : base(c)
            {
            }

            public EndTag(int c)
                : base(c)
            {
            }

            public EndTag(string tagName)
                : base(tagName)
            {
            }
        }

        #endregion

        #region Nested type: StartTag

        /// <summary>
        /// Start tag.
        /// </summary>
        public class StartTag : Tag
        {
            [Obsolete("", true)]
            public StartTag(char c)
                : base(c)
            {
            }

            public StartTag(int c)
                : base(c)
            {
            }

            public StartTag(string tagName)
                : base(tagName)
            {
            }
        }

        #endregion

        #region Nested type: Tag

        /// <summary>
        /// Start/end tag.
        /// </summary>
        public class Tag : Token
        {
            public List<Attribute> Attributes;
            public bool SelfClosing;
            private string _tagName;

            [Obsolete("", true)]
            // ReSharper disable UnusedParameter.Local
            public Tag(char c)
            // ReSharper restore UnusedParameter.Local
            {
            }

            public Tag(int c)
            {
                TagNameBuilder = new StringBuilder();
                TagNameBuilder.Append(Character.UnicodeToSurrogates(c));
            }

            public Tag(string tagName)
            {
                TagNameBuilder = new StringBuilder();
                TagNameBuilder.Append(tagName);
            }

            internal StringBuilder TagNameBuilder { get; private set; }

            // Displaying it nulls the builder, => crash at next iteration
            [DebuggerBrowsable(DebuggerBrowsableState.Never)]
            public string TagName
            {
                get
                {
                    if (_tagName == null)
                    {
                        _tagName = TagNameBuilder.ToString();
                        TagNameBuilder = null;
                    }
                    return _tagName;
                }
            }

            public bool HasAttributes
            {
                get { return Attributes != null && Attributes.Count > 0; }
            }

            /// <summary>
            /// Return the last attribute.
            /// </summary>
            public Attribute LastAttribute
            {
                get
                {
#if false
                    if (attributes == null)
                        attributes = new List<Attribute>();
                    if (attributes.Count == 0)
                        attributes.Add(new Attribute());
#endif
                    return Attributes[Attributes.Count - 1];
                }
            }

            [Obsolete("", true)]
            internal void AppendAttribute(char nameFirstChar)
            {
            }

            internal void AppendAttribute(int nameFirstChar)
            {
                if (Attributes == null)
                    Attributes = new List<Attribute>();
                Attributes.Add(new Attribute(Character.UnicodeToSurrogates(nameFirstChar)));
            }

            [Obsolete("", true)]
            public void AppendToName(char c)
            {
            }

            public void AppendToName(int c)
            {
                TagNameBuilder.Append(Character.UnicodeToSurrogates(c));
            }

            #region Nested type: Attribute

            public class Attribute
            {
                private string _name;
                private StringBuilder _nameBuilder = new StringBuilder();
                private string _value;
                private StringBuilder _valueBuilder = new StringBuilder();

                internal Attribute(string nameFirstChar)
                {
                    _nameBuilder.Append(nameFirstChar);
                }

                [DebuggerBrowsable(DebuggerBrowsableState.Never)]
                public string Name
                {
                    get
                    {
                        if (_name == null)
                        {
                            _name = _nameBuilder.ToString();
                            _nameBuilder = null;
                        }
                        return _name;
                    }
                }

                [DebuggerBrowsable(DebuggerBrowsableState.Never)]
                public string Value
                {
                    get
                    {
                        if (_value == null)
                        {
                            _value = _valueBuilder.ToString();
                            _valueBuilder = null;
                        }
                        return _value;
                    }
                }

                public override string ToString()
                {
                    return string.Format("{0}={1}", _name ?? _nameBuilder.ToString(), _value ?? _valueBuilder.ToString());
                }

                [Obsolete("", true)]
                public void AppendToName(char c)
                {
                }

                public void AppendToName(int c)
                {
                    _nameBuilder.Append(Character.UnicodeToSurrogates(c));
                }

                [Obsolete("", true)]
                public void AppendToValue(char c)
                {
                }

                public void AppendToValue(int c)
                {
                    _valueBuilder.Append(Character.UnicodeToSurrogates(c));
                }
            }

            #endregion
        }

        #endregion
    }

    public class ParseError : Token
    {
        private ParseError()
        {
        }

        public static ParseError GetSingleton()
        {
            return new ParseError();
        }
    }
}