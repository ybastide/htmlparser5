﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using HtmlParser5.Html;

namespace HtmlParser5
{
    /// <summary>
    /// Content models.
    /// </summary>
    public enum ContentModel
    {
        PcData,
        RcData,
        RawText,
        PlainText
    }

    public class Tokenizer : IEnumerable<Token>
    {
        private static readonly Dictionary<int, int> BadChar = new Dictionary<int, int>();

        private static readonly int EOF = HtmlCookedBuffer.EOF;

        /// <summary>
        /// Buffer.
        /// </summary>
        private readonly HtmlCookedBuffer _buffer;

        private readonly ParserState _parserState;

        /// <summary>
        /// Content model.
        /// </summary>
        public ContentModel ContentModel;

        /// <summary>
        /// Escape flag in RcData/RawText.
        /// </summary>
        private bool _escape;

        /// <summary>
        /// Tokenizer state.
        /// </summary>
        public TokenizerState State;

        static Tokenizer()
        {
            FillBadChars();
        }

        public Tokenizer(Stream stream, Encoding encoding, ParserState parserState)
        {
            _buffer = new HtmlCookedBuffer(stream, encoding);
            _parserState = parserState ?? new ParserState();
        }

        protected Tokenizer(HtmlCookedBuffer buffer)
        {
            _buffer = buffer;
            _parserState = new ParserState();
        }

        ///// <summary>
        ///// Parse error.
        ///// </summary>
        //public bool ParseError
        //{
        //    get { return _parserState.Error; }
        //    protected set { _parserState.Error = value; }
        //}

        #region FillBadChars
        private static void FillBadChars()
        {
            BadChar[0x00] = 0xFFFD;
            BadChar[0x0D] = 0x000A;
            BadChar[0x80] = 0x20AC;
            BadChar[0x81] = 0x0081;
            BadChar[0x82] = 0x201A;
            BadChar[0x83] = 0x0192;
            BadChar[0x84] = 0x201E;
            BadChar[0x85] = 0x2026;
            BadChar[0x86] = 0x2020;
            BadChar[0x87] = 0x2021;
            BadChar[0x88] = 0x02C6;
            BadChar[0x89] = 0x2030;
            BadChar[0x8A] = 0x0160;
            BadChar[0x8B] = 0x2039;
            BadChar[0x8C] = 0x0152;
            BadChar[0x8D] = 0x008D;
            BadChar[0x8E] = 0x017D;
            BadChar[0x8F] = 0x008F;
            BadChar[0x90] = 0x0090;
            BadChar[0x91] = 0x2018;
            BadChar[0x92] = 0x2019;
            BadChar[0x93] = 0x201C;
            BadChar[0x94] = 0x201D;
            BadChar[0x95] = 0x2022;
            BadChar[0x96] = 0x2013;
            BadChar[0x97] = 0x2014;
            BadChar[0x98] = 0x02DC;
            BadChar[0x99] = 0x2122;
            BadChar[0x9A] = 0x0161;
            BadChar[0x9B] = 0x203A;
            BadChar[0x9C] = 0x0153;
            BadChar[0x9D] = 0x009D;
            BadChar[0x9E] = 0x017E;
            BadChar[0x9F] = 0x0178;
            BadChar[0x000B] = 0x000B;
            BadChar[0xFFFE] = 0xFFFE;
            BadChar[0xFFFF] = 0xFFFF;
            BadChar[0x1FFFE] = 0x1FFFE;
            BadChar[0x1FFFF] = 0x1FFFF;
            BadChar[0x2FFFE] = 0x2FFFE;
            BadChar[0x2FFFF] = 0x2FFFF;
            BadChar[0x3FFFE] = 0x3FFFE;
            BadChar[0x3FFFF] = 0x3FFFF;
            BadChar[0x4FFFE] = 0x4FFFE;
            BadChar[0x4FFFF] = 0x4FFFF;
            BadChar[0x5FFFE] = 0x5FFFE;
            BadChar[0x5FFFF] = 0x5FFFF;
            BadChar[0x6FFFE] = 0x6FFFE;
            BadChar[0x6FFFF] = 0x6FFFF;
            BadChar[0x7FFFE] = 0x7FFFE;
            BadChar[0x7FFFF] = 0x7FFFF;
            BadChar[0x8FFFE] = 0x8FFFE;
            BadChar[0x8FFFF] = 0x8FFFF;
            BadChar[0x9FFFE] = 0x9FFFE;
            BadChar[0x9FFFF] = 0x9FFFF;
            BadChar[0xAFFFE] = 0xAFFFE;
            BadChar[0xAFFFF] = 0xAFFFF;
            BadChar[0xBFFFE] = 0xBFFFE;
            BadChar[0xBFFFF] = 0xBFFFF;
            BadChar[0xCFFFE] = 0xCFFFE;
            BadChar[0xCFFFF] = 0xCFFFF;
            BadChar[0xDFFFE] = 0xDFFFE;
            BadChar[0xDFFFF] = 0xDFFFF;
            BadChar[0xEFFFE] = 0xEFFFE;
            BadChar[0xEFFFF] = 0xEFFFF;
            BadChar[0xFFFFE] = 0xFFFFE;
            BadChar[0xFFFFF] = 0xFFFFF;
            BadChar[0x10FFFE] = 0x10FFFE;
            BadChar[0x10FFFF] = 0x10FFFF;
        }
        #endregion

        protected Token.StartTag LastStartTag;

        #region IEnumerable<Token> Membres

        public IEnumerator<Token> GetEnumerator()
        {
            Token.Tag currentTag = null;
            Token.Comment currentComment = null;
            Token.DocType currentDocType = null;
            List<int> temporaryBuffer = null;

            while (State != TokenizerState.EOF)
            {
                int c;
                bool isBadChar;
                switch (State)
                {
                    case TokenizerState.Data:
                        c = GetChar(out isBadChar);
                        if (c == '&' && (ContentModel == ContentModel.PcData || ContentModel == ContentModel.RcData)
                            && !_escape)
                        {
                            State = TokenizerState.CharacterReference;
                            break;
                        }
                        if (c == '-' && (ContentModel == ContentModel.RcData || ContentModel == ContentModel.RawText)
                            && !_escape && _buffer.EndsWith("<!--"))
                        {
                            _escape = true;
                        }
                        else if (c == '<')
                        {
                            if (ContentModel == ContentModel.PcData
                                || ((ContentModel == ContentModel.RcData || ContentModel == ContentModel.RawText)
                                    && !_escape))
                            {
                                State = ContentModel == ContentModel.PcData ? TokenizerState.TagOpen : TokenizerState.RCDATALessThan;
                                break;
                            }
                        }
                        else if (c == '>' && _escape
                                 && (ContentModel == ContentModel.RcData || ContentModel == ContentModel.RawText)
                                 && _buffer.EndsWith("-->"))
                        {
                            _escape = false;
                        }
                        if (c == EOF)
                        {
                            yield return new Token.EOF();
                            State = TokenizerState.EOF;
                        }
                        else
                        {
                            // XXX pas sûr
                            if (isBadChar)
                                yield return Token.ParseError;
                            yield return new Token.Character(c);
                        }
                        break;
                    case TokenizerState.CharacterReference:
                        {
                            if (ContentModel == ContentModel.RawText)
                                throw new Exception("Can't happen 163");
                            int c2;
                            bool isParseError;
                            if (!ConsumeCharRef(out c, out c2, new char[0], false, out isParseError, out isBadChar))
                            {
                                c = '&';
                            }
                            if (isParseError)
                                yield return Token.ParseError;
                            if (isBadChar)
                                yield return Token.ParseError;
                            State = TokenizerState.Data;
                            yield return new Token.Character(c);
                            if (c2 != -1)
                                yield return new Token.Character(c2);
                        }
                        break;
                    case TokenizerState.TagOpen:
                        //if (ContentModel == ContentModel.RcData || ContentModel == ContentModel.RawText)
                        //{
                        //    c = GetChar(out isBadChar);
                        //    if (c == '/')
                        //    {
                        //        State = TokenizerState.CloseTagOpen;
                        //    }
                        //    else
                        //    {
                        //        yield return new Token.Character('<');
                        //        State = TokenizerState.Data;
                        //        _buffer.Put(c);
                        //        break;
                        //    }
                        //}
                        //else
                        if (ContentModel == ContentModel.PcData)
                        {
                            c = _buffer.Get();
                            switch (c)
                            {
                                case '!':
                                    State = TokenizerState.MarkupDeclarationOpen;
                                    break;
                                case '/':
                                    State = TokenizerState.CloseTagOpen;
                                    break;
                                case '>':
                                    yield return Token.ParseError;
                                    yield return new Token.Character('<');
                                    yield return new Token.Character('>');
                                    State = TokenizerState.Data;
                                    break;
                                case '?':
                                    yield return Token.ParseError;
                                    _buffer.Put(c);
                                    State = TokenizerState.BogusComment;
                                    break;
                                default:
                                    if (IsAlpha(c))
                                    {
                                        currentTag = LastStartTag = new Token.StartTag(c | 32);
                                        State = TokenizerState.TagName;
                                    }
                                    else
                                    {
                                        yield return Token.ParseError;
                                        yield return new Token.Character('<');
                                        State = TokenizerState.Data;
                                        _buffer.Put(c);
                                        break;
                                    }
                                    break;
                            }
                        }
                        else
                            throw new Exception("Can't happen 189");
                        break;
                    case TokenizerState.CloseTagOpen:
                        if (ContentModel == ContentModel.RcData || ContentModel == ContentModel.RawText)
                        {
                            if (LastStartTag == null || !MatchTagAndEnd(LastStartTag))
                            {
                                yield return new Token.Character('<');
                                yield return new Token.Character('/');
                                State = TokenizerState.Data;
                                break;
                            }
                        }
                        c = _buffer.Get();
                        if (IsAlpha(c))
                        {
                            currentTag = new Token.EndTag(c | 32);
                            State = TokenizerState.TagName;
                        }
                        else if (c == '>')
                        {
                            yield return Token.ParseError;
                            State = TokenizerState.Data;
                        }
                        else if (c == EOF)
                        {
                            yield return Token.ParseError;
                            yield return new Token.Character('<');
                            yield return new Token.Character('/');
                            State = TokenizerState.Data;
                        }
                        else
                        {
                            yield return Token.ParseError;
                            _buffer.Put(c);
                            State = TokenizerState.BogusComment;
                        }
                        break;
                    case TokenizerState.TagName:
                        //c = _buffer.Get();
                        c = GetChar(out isBadChar);
                        if (IsSpace(c))
                            State = TokenizerState.BeforeAttributeName;
                        else if (c == '/')
                            State = TokenizerState.SelfClosingStartTag;
                        else if (c == '>')
                        {
                            yield return currentTag;
                            if (currentTag is Token.EndTag)
                                ContentModel = ContentModel.PcData;
                            currentTag = null;
                            State = TokenizerState.Data;
                        }
                        else if (IsUpper(c))
                            currentTag.AppendToName(c | 32);
                        else if (c == EOF)
                        {
                            yield return Token.ParseError;
                            currentTag = null;
                            State = TokenizerState.Data;
                        }
                        else
                        {
                            //if (c == 0)
                            //{
                            //    yield return Token.ParseError;
                            //    c = 0xfffd;
                            //}
                            if (isBadChar)
                                yield return Token.ParseError;
                            currentTag.AppendToName(c);
                        }
                        break;
                    case TokenizerState.BeforeAttributeName:
                        c = GetChar(out isBadChar);
                        if (IsSpace(c))
                        {
                            /* Do nothing */
                        }
                        else if (c == '/')
                        {
                            if (RemoveLastAttributeIfDup(currentTag.Attributes))
                                yield return Token.ParseError;
                            State = TokenizerState.SelfClosingStartTag;
                        }
                        else if (c == '>')
                        {
                            if (RemoveLastAttributeIfDup(currentTag.Attributes)/* && !(currentTag is Token.EndTag)*/)
                                yield return Token.ParseError;
                            if (currentTag is Token.EndTag && currentTag.HasAttributes)
                                yield return Token.ParseError;
                            yield return currentTag;
                            if (currentTag is Token.EndTag)
                                ContentModel = ContentModel.PcData;
                            currentTag = null;
                            State = TokenizerState.Data;
                        }
                        else if (IsUpper(c))
                        {
                            currentTag.AppendAttribute(c | 32);
                            State = TokenizerState.AttributeName;
                        }
                        else if (c == EOF)
                        {
                            yield return Token.ParseError;
                            currentTag = null;
                            State = TokenizerState.Data;
                        }
                        else
                        {
                            if (c == '"' || c == '\'' || c == '<' || c == '=')
                                yield return Token.ParseError;
                            if (isBadChar)
                                yield return Token.ParseError;
                            currentTag.AppendAttribute(c);
                            State = TokenizerState.AttributeName;
                        }
                        break;
                    case TokenizerState.AttributeName:
                        c = GetChar(out isBadChar);
                        if (IsSpace(c))
                        {
                            State = TokenizerState.AfterAttributeName;
                        }
                        else if (c == '/')
                        {
                            if (RemoveLastAttributeIfDup(currentTag.Attributes)/* && !(currentTag is Token.EndTag)*/)
                                yield return Token.ParseError;
                            State = TokenizerState.SelfClosingStartTag;
                        }
                        else if (c == '=')
                        {
                            State = TokenizerState.BeforeAttributeValue;
                        }
                        else if (c == '>')
                        {
                            if (RemoveLastAttributeIfDup(currentTag.Attributes)/* && !(currentTag is Token.EndTag)*/)
                                yield return Token.ParseError;
                            if (currentTag is Token.EndTag && currentTag.HasAttributes)
                                yield return Token.ParseError;
                            yield return currentTag;
                            if (currentTag is Token.EndTag)
                                ContentModel = ContentModel.PcData;
                            currentTag = null;
                            State = TokenizerState.Data;
                        }
                        else if (IsUpper(c))
                        {
                            currentTag.LastAttribute.AppendToName(c | 32);
                        }
                        else if (c == EOF)
                        {
                            yield return Token.ParseError;
                            currentTag = null;
                            State = TokenizerState.Data;
                        }
                        else
                        {
                            if (c == '"' || c == '\'' || c == '<' || isBadChar)
                                yield return Token.ParseError;
                            currentTag.LastAttribute.AppendToName(c);
                        }
                        break;
                    case TokenizerState.AfterAttributeName:
                        c = GetChar(out isBadChar);
                        if (IsSpace(c))
                        {
                            /* Do nothing */
                        }
                        else if (c == '/')
                        {
                            if (RemoveLastAttributeIfDup(currentTag.Attributes)/* && !(currentTag is Token.EndTag)*/)
                                yield return Token.ParseError;
                            State = TokenizerState.SelfClosingStartTag;
                        }
                        else if (c == '=')
                        {
                            State = TokenizerState.BeforeAttributeValue;
                        }
                        else if (c == '>')
                        {
                            if (RemoveLastAttributeIfDup(currentTag.Attributes)/* && !(currentTag is Token.EndTag)*/)
                                yield return Token.ParseError;
                            if (currentTag is Token.EndTag && currentTag.HasAttributes)
                                yield return Token.ParseError;
                            yield return currentTag;
                            if (currentTag is Token.EndTag)
                                ContentModel = ContentModel.PcData;
                            currentTag = null;
                            State = TokenizerState.Data;
                        }
                        else if (IsUpper(c))
                        {
                            currentTag.AppendAttribute(c | 32);
                            State = TokenizerState.AttributeName;
                        }
                        else if (c == EOF)
                        {
                            yield return Token.ParseError;
                            currentTag = null;
                            State = TokenizerState.Data;
                        }
                        else
                        {
                            if (c == '"' || c == '\'' || c == '<' || isBadChar)
                                yield return Token.ParseError;
                            currentTag.AppendAttribute(c);
                            State = TokenizerState.AttributeName;
                        }
                        break;
                    case TokenizerState.BeforeAttributeValue:
                        c = GetChar(out isBadChar);
                        if (IsSpace(c))
                        {
                            /* Do nothing */
                        }
                        else if (c == '"')
                            State = TokenizerState.AttributeValueDoubleQuoted;
                        else if (c == '\'')
                            State = TokenizerState.AttributeValueSingleQuoted;
                        else if (c == '&')
                        {
                            _buffer.Put(c);
                            State = TokenizerState.AttributeValueUnquoted;
                        }
                        else if (c == '>')
                        {
                            yield return Token.ParseError;
                            RemoveLastAttributeIfDup(currentTag.Attributes);
                            yield return currentTag;
                            if (currentTag is Token.EndTag)
                                ContentModel = ContentModel.PcData;
                            currentTag = null;
                            State = TokenizerState.Data;
                        }
                        else if (c == EOF)
                        {
                            yield return Token.ParseError;
                            currentTag = null;
                            State = TokenizerState.Data;
                        }
                        else
                        {
                            if (c == '=' || c == '<' || c == '`' || isBadChar)
                                yield return Token.ParseError;
                            currentTag.LastAttribute.AppendToValue(c);
                            State = TokenizerState.AttributeValueUnquoted;
                        }

                        break;
                    case TokenizerState.AttributeValueDoubleQuoted:
                        c = GetChar(out isBadChar);
                        if (c == '"')
                            State = TokenizerState.AfterAttributeValueQuoted;
                        else if (c == '&')
                        {
                            int c2;
                            bool isParseError;
                            if (!ConsumeCharRef(out c, out c2, new[] { '"' }, true, out isParseError, out isBadChar))
                                c = '&';
                            if (isParseError)
                                yield return Token.ParseError;
                            if (isBadChar)
                                yield return Token.ParseError;
                            currentTag.LastAttribute.AppendToValue(c);
                            if (c2 != -1)
                                currentTag.LastAttribute.AppendToValue(c2);
                        }
                        else if (c == EOF)
                        {
                            yield return Token.ParseError;
                            currentTag = null;
                            State = TokenizerState.Data;
                        }
                        else
                        {
                            if (isBadChar)
                                yield return Token.ParseError;
                            currentTag.LastAttribute.AppendToValue(c);
                        }
                        break;
                    case TokenizerState.AttributeValueSingleQuoted:
                        c = GetChar(out isBadChar);
                        if (c == '\'')
                            State = TokenizerState.AfterAttributeValueQuoted;
                        else if (c == '&')
                        {
                            int c2;
                            bool isParseError;
                            if (!ConsumeCharRef(out c, out c2, new[] { '\'' }, true, out isParseError, out isBadChar))
                                c = '&';
                            if (isParseError)
                                yield return Token.ParseError;
                            if (isBadChar)
                                yield return Token.ParseError;
                            currentTag.LastAttribute.AppendToValue(c);
                            if (c2 != -1)
                                currentTag.LastAttribute.AppendToValue(c2);
                        }
                        else if (c == EOF)
                        {
                            yield return Token.ParseError;
                            currentTag = null;
                            State = TokenizerState.Data;
                        }
                        else
                        {
                            if (isBadChar)
                                yield return Token.ParseError;
                            currentTag.LastAttribute.AppendToValue(c);
                        }
                        break;
                    case TokenizerState.AttributeValueUnquoted:
                        c = GetChar(out isBadChar);
                        if (IsSpace(c))
                        {
                            if (RemoveLastAttributeIfDup(currentTag.Attributes)/* && !(currentTag is Token.EndTag)*/)
                                yield return Token.ParseError;
                            State = TokenizerState.BeforeAttributeName;
                        }
                        else if (c == '&')
                        {
                            int c2;
                            bool isParseError;
                            if (!ConsumeCharRef(out c, out c2, new char[0], true, out isParseError, out isBadChar))
                                c = '&';
                            if (isParseError)
                                yield return Token.ParseError;
                            if (isBadChar)
                                yield return Token.ParseError;
                            currentTag.LastAttribute.AppendToValue(c);
                            if (c2 != -1)
                                currentTag.LastAttribute.AppendToValue(c2);
                        }
                        else if (c == '>')
                        {
                            if (RemoveLastAttributeIfDup(currentTag.Attributes)/* && !(currentTag is Token.EndTag)*/)
                                yield return Token.ParseError;
                            if (currentTag is Token.EndTag && currentTag.HasAttributes)
                                yield return Token.ParseError;
                            yield return currentTag;
                            if (currentTag is Token.EndTag)
                                ContentModel = ContentModel.PcData;
                            currentTag = null;
                            State = TokenizerState.Data;
                        }
                        else if (c == EOF)
                        {
                            yield return Token.ParseError;
                            currentTag = null;
                            State = TokenizerState.Data;
                        }
                        else
                        {
                            if (c == '"' || c == '\'' || c == '<' || c == '=' || c == '`')
                                yield return Token.ParseError;
                            if (isBadChar)
                                yield return Token.ParseError;
                            currentTag.LastAttribute.AppendToValue(c);
                        }
                        break;
                    //case TokenizerState.CharacterReferenceInAttributeValue:
                    //break;
                    case TokenizerState.AfterAttributeValueQuoted:
                        c = GetChar(out isBadChar);
                        if (IsSpace(c))
                        {
                            if (RemoveLastAttributeIfDup(currentTag.Attributes)/* && !(currentTag is Token.EndTag)*/)
                                yield return Token.ParseError;
                            State = TokenizerState.BeforeAttributeName;
                        }
                        else if (c == '/')
                        {
                            if (RemoveLastAttributeIfDup(currentTag.Attributes)/* && !(currentTag is Token.EndTag)*/)
                                yield return Token.ParseError;
                            State = TokenizerState.SelfClosingStartTag;
                        }
                        else if (c == '>')
                        {
                            if (RemoveLastAttributeIfDup(currentTag.Attributes)/* && !(currentTag is Token.EndTag)*/)
                                yield return Token.ParseError;
                            if (currentTag is Token.EndTag && currentTag.HasAttributes)
                                yield return Token.ParseError;
                            yield return currentTag;
                            if (currentTag is Token.EndTag)
                                ContentModel = ContentModel.PcData;
                            currentTag = null;
                            State = TokenizerState.Data;
                        }
                        else if (c == EOF)
                        {
                            yield return Token.ParseError;
                            currentTag = null;
                            State = TokenizerState.Data;
                        }
                        else
                        {
                            if (isBadChar)
                                yield return Token.ParseError;
                            yield return Token.ParseError;
                            _buffer.Put(c);
                            State = TokenizerState.BeforeAttributeName;
                        }
                        break;
                    case TokenizerState.SelfClosingStartTag:
                        c = GetChar(out isBadChar);
                        if (c == '>')
                        {
                            currentTag.SelfClosing = true;
                            if (currentTag is Token.EndTag)
                                yield return Token.ParseError;
                            yield return currentTag;
                            if (currentTag is Token.EndTag)
                                ContentModel = ContentModel.PcData;
                            currentTag = null;
                            State = TokenizerState.Data;
                        }
                        else if (c == EOF)
                        {
                            yield return Token.ParseError;
                            State = TokenizerState.Data;
                        }
                        else
                        {
                            if (isBadChar)
                                yield return Token.ParseError;
                            yield return Token.ParseError;
                            _buffer.Put(c);
                            State = TokenizerState.BeforeAttributeName;
                        }
                        break;
                    case TokenizerState.BogusComment:
                        if (ContentModel != ContentModel.PcData)
                            throw new Exception("Can't happen 571");
                        {
                            var sb = new StringBuilder();
                            while ((c = GetChar(out isBadChar)) != EOF && c != '>')
                            {
                                //if (c == '\u000B')
                                if (isBadChar)
                                    yield return Token.ParseError;
                                //if (c == 0)
                                //    c = 0xfffd;
                                sb.Append((char)c);
                            }
                            if (c != '>')
                                _buffer.Put(c);
                            State = TokenizerState.Data;
                            yield return new Token.Comment(sb);
                        }
                        break;
                    case TokenizerState.MarkupDeclarationOpen:
                        if (ContentModel != ContentModel.PcData)
                            throw new Exception("Can't happen 583");
                        {
                            //var sb = new StringBuilder();
                            if (BufferMatch("--", false))
                            {
                                currentComment = new Token.Comment();
                                State = TokenizerState.CommentStart;
                            }
                            else if (BufferMatch("doctype", true))
                            {
                                State = TokenizerState.DOCTYPE;
                            }
                            else if (_parserState.InsertionMode == InsertionMode.InForeignContent
                                     && !(_parserState.CurrentNode is Element)
                                     && BufferMatch("[CDATA[", false))
                            {
                                State = TokenizerState.CDATASection;
                            }
                            else
                            {
                                yield return Token.ParseError;
                                State = TokenizerState.BogusComment;
                            }
                        }
                        break;
                    case TokenizerState.CommentStart:
                        c = GetChar(out isBadChar);
                        //c = _buffer.Get();
                        if (c == '-')
                            State = TokenizerState.CommentStartDash;
                        else if (c == '>' || c == EOF)
                        {
                            yield return Token.ParseError;
                            yield return currentComment;
                            State = TokenizerState.Data;
                            currentComment = null;
                        }
                        else
                        {
                            if (isBadChar)
                                yield return Token.ParseError;
                            // XXX
                            //if (c == '\v')
                            //    yield return Token.ParseError;
                            //if (c == 0)
                            //{
                            //    yield return Token.ParseError;
                            //    c = 0xfffd;
                            //}
                            currentComment.Append(c);
                            State = TokenizerState.Comment;
                        }
                        break;
                    case TokenizerState.CommentStartDash:
                        //c = _buffer.Get();
                        c = GetChar(out isBadChar);
                        if (c == '-')
                            State = TokenizerState.CommentEnd;
                        else if (c == '>' || c == EOF)
                        {
                            yield return Token.ParseError;
                            yield return currentComment;
                            State = TokenizerState.Data;
                            currentComment = null;
                        }
                        else
                        {
                            currentComment.Append('-');
                            if (isBadChar)
                                yield return Token.ParseError;
                            //// XXX
                            //if (c == '\v')
                            //    yield return Token.ParseError;
                            //if (c == 0)
                            //{
                            //    yield return Token.ParseError;
                            //    c = 0xfffd;
                            //}
                            currentComment.Append(c);
                            State = TokenizerState.Comment;
                        }
                        break;
                    case TokenizerState.Comment:
                        //c = _buffer.Get();
                        c = GetChar(out isBadChar);
                        if (c == '-')
                            State = TokenizerState.CommentEndDash;
                        else if (c == EOF)
                        {
                            yield return Token.ParseError;
                            yield return currentComment;
                            State = TokenizerState.Data;
                            currentComment = null;
                        }
                        else
                        {
                            if (isBadChar)
                                yield return Token.ParseError;
                            //// XXX
                            //if (c == '\v')
                            //    yield return Token.ParseError;
                            //if (c == 0)
                            //{
                            //    yield return Token.ParseError;
                            //    c = 0xfffd;
                            //}
                            currentComment.Append(c);
                        }
                        break;
                    case TokenizerState.CommentEndDash:
                        c = GetChar(out isBadChar);
                        if (c == '-')
                            State = TokenizerState.CommentEnd;
                        else if (c == EOF)
                        {
                            yield return Token.ParseError;
                            yield return currentComment;
                            State = TokenizerState.Data;
                            currentComment = null;
                        }
                        else
                        {
                            currentComment.Append('-');
                            if (isBadChar)
                                yield return Token.ParseError;
                            //// XXX
                            //if (c == '\v')
                            //    yield return Token.ParseError;
                            //if (c == 0)
                            //{
                            //    yield return Token.ParseError;
                            //    c = 0xfffd;
                            //}
                            currentComment.Append(c);
                            State = TokenizerState.Comment;
                        }
                        break;
                    case TokenizerState.CommentEnd:
                        c = GetChar(out isBadChar);
                        if (c == '>')
                        {
                            yield return currentComment;
                            State = TokenizerState.Data;
                            currentComment = null;
                        }
                        else if (c == '-')
                        {
                            yield return Token.ParseError;
                            currentComment.Append('-');
                        }
                        // n'existe plus
                        //else if (IsSpace(c))
                        //{
                        //    yield return Token.ParseError;
                        //    currentComment.Content.Append("--");
                        //    State = TokenizerState.CommentEndSpace;
                        //}
                        else if (c == '!')
                        {
                            // XXX domjs.test refuse ce parse error ?!
                            yield return Token.ParseError;
                            State = TokenizerState.CommentEndBang;
                        }
                        else if (c == EOF)
                        {
                            yield return Token.ParseError;
                            yield return currentComment;
                            State = TokenizerState.Data;
                            currentComment = null;
                        }
                        else
                        {
                            if (isBadChar)
                                yield return Token.ParseError;
                            //// XXX
                            //if (c == '\v')
                            //    yield return Token.ParseError;
                            //if (c == 0)
                            //{
                            //    //yield return Token.ParseError;
                            //    c = 0xfffd;
                            //}
                            yield return Token.ParseError;
                            currentComment.Append("--");
                            currentComment.Append(c);
                            State = TokenizerState.Comment;
                        }
                        break;
                    case TokenizerState.CommentEndBang:
                        c = GetChar(out isBadChar);
                        if (c == '>' || c == EOF)
                        {
                            if (c == EOF)
                                yield return Token.ParseError;
                            yield return currentComment;
                            State = TokenizerState.Data;
                            currentComment = null;
                        }
                        else if (c == '-')
                        {
                            currentComment.Append("--!");
                            State = TokenizerState.CommentEndDash;
                        }
                        else
                        {
                            if (isBadChar)
                                yield return Token.ParseError;
                            //if (c == 0)
                            //{
                            //    //yield return Token.ParseError;
                            //    c = 0xfffd;
                            //}
                            currentComment.Append("--!");
                            currentComment.Append(c);
                            State = TokenizerState.Comment;
                        }
                        break;
                    case TokenizerState.CommentEndSpace:
                        c = GetChar(out isBadChar);
                        if (c == '>' || c == EOF)
                        {
                            if (c == EOF)
                                yield return Token.ParseError;
                            yield return currentComment;
                            State = TokenizerState.Data;
                            currentComment = null;
                        }
                        else if (c == '-')
                        {
                            State = TokenizerState.CommentEndDash;
                        }
                        else if (IsSpace(c))
                        {
                            currentComment.Append(c);
                        }
                        else
                        {
                            currentComment.Append(c);
                            State = TokenizerState.Comment;
                        }
                        break;
                    case TokenizerState.DOCTYPE:
                        c = GetChar(out isBadChar);
                        if (IsSpace(c))
                        {
                            State = TokenizerState.BeforeDOCTYPEName;
                        }
                        else if (c == EOF)
                        {
                            State = TokenizerState.Data;
                            yield return Token.ParseError;
                            yield return new Token.DocType(true);
                        }
                        else
                        {
                            if (isBadChar)
                                yield return Token.ParseError;
                            //if (c == 0)
                            //{
                            //    yield return Token.ParseError;
                            //    //if (c == 0)
                            //    c = 0xfffd;
                            //}
                            yield return Token.ParseError;
                            State = TokenizerState.BeforeDOCTYPEName;
                            _buffer.Put(c);
                        }
                        break;
                    case TokenizerState.BeforeDOCTYPEName:
                        c = GetChar(out isBadChar);
                        if (IsSpace(c))
                        {
                            /* Nothing */
                        }
                        else if (c == '>' || c == EOF)
                        {
                            State = TokenizerState.Data;
                            yield return Token.ParseError;
                            yield return new Token.DocType(true);
                        }
                        else
                        {
                            if (IsUpper(c))
                                c += 32;
                            if (isBadChar)
                                yield return Token.ParseError;
                            //if (c < ' ')
                            //{
                            //    yield return Token.ParseError;
                            //    if (c == 0)
                            //        c = 0xfffd;
                            //}
                            currentDocType = new Token.DocType();
                            currentDocType.AppendToName(c);
                            State = TokenizerState.DOCTYPEName;
                        }
                        break;
                    case TokenizerState.DOCTYPEName:
                        c = GetChar(out isBadChar);
                        if (IsSpace(c))
                        {
                            State = TokenizerState.AfterDOCTYPEName;
                        }
                        else if (c == '>' || c == EOF)
                        {
                            if (c == EOF)
                            {
                                yield return Token.ParseError;
                                currentDocType.ForceQuirks = true;
                            }
                            State = TokenizerState.Data;
                            yield return currentDocType;
                            currentDocType = null;
                        }
                        else
                        {
                            if (isBadChar)
                                yield return Token.ParseError;
                            //if (c < ' ')
                            //{
                            //    yield return Token.ParseError;
                            //    if (c == 0)
                            //        c = 0xfffd;
                            //}
                            if (IsUpper(c))
                                c += 32;
                            currentDocType.AppendToName(c);
                        }
                        break;
                    case TokenizerState.AfterDOCTYPEName:
                        c = GetChar(out isBadChar);
                        if (IsSpace(c))
                        {
                            /* Nothing */
                        }
                        else if (c == '>' || c == EOF)
                        {
                            if (c == EOF)
                            {
                                yield return Token.ParseError;
                                currentDocType.ForceQuirks = true;
                            }
                            State = TokenizerState.Data;
                            yield return currentDocType;
                            currentDocType = null;
                        }
                        else
                        {
                            if (isBadChar)
                                yield return Token.ParseError;
                            //if (c < ' ')
                            //{
                            //    //yield return Token.ParseError;
                            //    if (c == 0)
                            //        c = 0xfffd;
                            //    else
                            //        yield return Token.ParseError;
                            //}
                            _buffer.Put(c);
                            if (BufferMatch("public", true))
                                State = TokenizerState.AfterDOCTYPEPublicKeyword;
                            else if (BufferMatch("system", true))
                                State = TokenizerState.AfterDOCTYPESystemKeyword;
                            else
                            {
                                yield return Token.ParseError;
                                // XXX à vérifier sur les autres < ' '
                                currentDocType.ForceQuirks = true;
                                State = TokenizerState.BogusDOCTYPE;
                            }
                        }
                        break;
                    case TokenizerState.AfterDOCTYPEPublicKeyword:
                    case TokenizerState.BeforeDOCTYPEPublicIdentifier:
                        c = GetChar(out isBadChar);
                        if (IsSpace(c))
                        {
                            State = TokenizerState.BeforeDOCTYPEPublicIdentifier;
                        }
                        else if (c == '"')
                        {
                            if (State == TokenizerState.AfterDOCTYPEPublicKeyword)
                                yield return Token.ParseError;
                            currentDocType.InitializePI();
                            State = TokenizerState.DOCTYPEPublicIdentifierDoubleQuoted;
                        }
                        else if (c == '\'')
                        {
                            if (State == TokenizerState.AfterDOCTYPEPublicKeyword)
                                yield return Token.ParseError;
                            currentDocType.InitializePI();
                            State = TokenizerState.DOCTYPEPublicIdentifierSingleQuoted;
                        }
                        else if (c == '>' || c == EOF)
                        {
                            yield return Token.ParseError;
                            currentDocType.ForceQuirks = true;
                            State = TokenizerState.Data;
                            yield return currentDocType;
                            currentDocType = null;
                        }
                        else
                        {
                            if (isBadChar)
                            {
                                // XXX
                                if (c != 0)
                                    yield return Token.ParseError;
                            }
                            yield return Token.ParseError;
                            currentDocType.ForceQuirks = true;
                            State = TokenizerState.BogusDOCTYPE;
                        }
                        break;
                    //case TokenizerState.BeforeDOCTYPEPublicIdentifier:
                    //c = GetChar(out isBadChar);
                    //if (IsSpace(c))
                    //{
                    //    // rien
                    //}
                    //else if (c == '"')
                    //{
                    //    currentDocType.InitializePI();
                    //    State = TokenizerState.DOCTYPEPublicIdentifierDoubleQuoted;
                    //}
                    //else if (c == '\'')
                    //{
                    //    currentDocType.InitializePI();
                    //    State = TokenizerState.DOCTYPEPublicIdentifierSingleQuoted;
                    //}
                    //else if (c == '>' || c == EOF)
                    //{
                    //    yield return Token.ParseError;
                    //    currentDocType.ForceQuirks = true;
                    //    State = TokenizerState.Data;
                    //    yield return currentDocType;
                    //    currentDocType = null;
                    //}
                    //else
                    //{
                    //    if (isBadChar)
                    //    {
                    //        if (c != 0)
                    //            yield return Token.ParseError;
                    //    }
                    //    yield return Token.ParseError;
                    //    currentDocType.ForceQuirks = true;
                    //    State = TokenizerState.BogusDOCTYPE;
                    //}
                    //break;
                    case TokenizerState.DOCTYPEPublicIdentifierDoubleQuoted:
                        c = GetChar(out isBadChar);
                        if (c == '"')
                            State = TokenizerState.AfterDOCTYPEPublicIdentifier;
                        else if (c == '>' || c == EOF)
                        {
                            yield return Token.ParseError;
                            currentDocType.ForceQuirks = true;
                            State = TokenizerState.Data;
                            yield return currentDocType;
                            currentDocType = null;
                        }
                        else
                        {
                            if (isBadChar)
                                yield return Token.ParseError;
                            //if (c < ' ')
                            //{
                            //    //if (c != 0)
                            //    yield return Token.ParseError;
                            //    if (c == 0)
                            //        c = 0xfffd;
                            //}
                            currentDocType.AppendToPI(c);
                        }
                        break;
                    case TokenizerState.DOCTYPEPublicIdentifierSingleQuoted:
                        c = GetChar(out isBadChar);
                        if (c == '\'')
                            State = TokenizerState.AfterDOCTYPEPublicIdentifier;
                        else if (c == '>' || c == EOF)
                        {
                            yield return Token.ParseError;
                            currentDocType.ForceQuirks = true;
                            State = TokenizerState.Data;
                            yield return currentDocType;
                            currentDocType = null;
                        }
                        else
                        {
                            if (isBadChar)
                                yield return Token.ParseError;
                            //if (c < ' ')
                            //{
                            //    if (c != 0)
                            //        yield return Token.ParseError;
                            //    if (c == 0)
                            //        c = 0xfffd;
                            //}
                            currentDocType.AppendToPI(c);
                        }
                        break;
                    case TokenizerState.AfterDOCTYPEPublicIdentifier:
                    case TokenizerState.AfterDOCTYPESystemKeyword:
                    case TokenizerState.BetweenDOCTYPEPublicAndSystemIdentifiers:
                        c = GetChar(out isBadChar);
                        if (IsSpace(c))
                        {
                            State = TokenizerState.BetweenDOCTYPEPublicAndSystemIdentifiers;
                        }
                        else if (c == '"')
                        {
                            if (State != TokenizerState.BetweenDOCTYPEPublicAndSystemIdentifiers)
                                yield return Token.ParseError;
                            currentDocType.InitializeSI();
                            State = TokenizerState.DOCTYPESystemIdentifierDoubleQuoted;
                        }
                        else if (c == '\'')
                        {
                            if (State != TokenizerState.BetweenDOCTYPEPublicAndSystemIdentifiers)
                                yield return Token.ParseError;
                            currentDocType.InitializeSI();
                            State = TokenizerState.DOCTYPESystemIdentifierSingleQuoted;
                        }
                        else if (c == '>')
                        {
                            if (State == TokenizerState.AfterDOCTYPESystemKeyword)
                            {
                                yield return Token.ParseError;
                                currentDocType.ForceQuirks = true;
                            }
                            State = TokenizerState.Data;
                            yield return currentDocType;
                            currentDocType = null;
                        }
                        else if (c == EOF)
                        {
                            yield return Token.ParseError;
                            currentDocType.ForceQuirks = true;
                            State = TokenizerState.Data;
                            yield return currentDocType;
                            currentDocType = null;
                        }
                        else
                        {
                            if (isBadChar)
                                yield return Token.ParseError;
                            yield return Token.ParseError;
                            currentDocType.ForceQuirks = true;
                            State = TokenizerState.BogusDOCTYPE;
                        }
                        break;
                    case TokenizerState.DOCTYPESystemIdentifierDoubleQuoted:
                        c = GetChar(out isBadChar);
                        if (c == '"')
                            State = TokenizerState.AfterDOCTYPESystemIdentifier;
                        else if (c == '>' || c == EOF)
                        {
                            yield return Token.ParseError;
                            currentDocType.ForceQuirks = true;
                            State = TokenizerState.Data;
                            yield return currentDocType;
                            currentDocType = null;
                        }
                        else
                        {
                            if (isBadChar)
                                yield return Token.ParseError;
                            //if (c == 0)
                            //{
                            //    yield return Token.ParseError;
                            //    c = 0xfffd;
                            //}
                            currentDocType.AppendToSI(c);
                        }
                        break;
                    case TokenizerState.DOCTYPESystemIdentifierSingleQuoted:
                        c = GetChar(out isBadChar);
                        if (c == '\'')
                            State = TokenizerState.AfterDOCTYPESystemIdentifier;
                        else if (c == '>' || c == EOF)
                        {
                            yield return Token.ParseError;
                            currentDocType.ForceQuirks = true;
                            State = TokenizerState.Data;
                            yield return currentDocType;
                            currentDocType = null;
                        }
                        else
                        {
                            if (isBadChar)
                                yield return Token.ParseError;
                            //if (c == 0)
                            //{
                            //    yield return Token.ParseError;
                            //    c = 0xfffd;
                            //}
                            currentDocType.AppendToSI(c);
                        }
                        break;
                    case TokenizerState.AfterDOCTYPESystemIdentifier:
                        c = GetChar(out isBadChar);
                        if (IsSpace(c))
                        {
                            /* Nothing */
                        }
                        else if (c == '>' || c == EOF)
                        {
                            if (c == EOF)
                            {
                                yield return Token.ParseError;
                                currentDocType.ForceQuirks = true;
                            }
                            State = TokenizerState.Data;
                            yield return currentDocType;
                            currentDocType = null;
                        }
                        else
                        {
                            if (isBadChar)
                                yield return Token.ParseError;
                            yield return Token.ParseError;
                            State = TokenizerState.BogusDOCTYPE;
                        }
                        break;
                    case TokenizerState.BogusDOCTYPE:
                        while ((c = GetChar(out isBadChar)) != '>' && c != EOF)
                        {
                            // nothing?
                            if (isBadChar)
                                yield return Token.ParseError;
                        }
                        State = TokenizerState.Data;
                        yield return currentDocType;
                        currentDocType = null;
                        break;
                    case TokenizerState.CDATASection:
                        if (ContentModel != ContentModel.PcData)
                            throw new Exception("Can't happen 1019");
                        while (!BufferMatch("]]>", false))
                        {
                            c = GetChar(out isBadChar);
                            if (c == EOF)
                                break;
                            yield return new Token.Character(c);
                        }
                        State = TokenizerState.Data;
                        break;
                    case TokenizerState.RCDATALessThan:
                        c = GetChar(out isBadChar);
                        if (c == '/' && LastStartTag != null)
                        {
                            temporaryBuffer = new List<int>();
                            State = TokenizerState.RCDATAEndTagOpen;
                        }
                        else
                        {
                            yield return new Token.Character('<');
                            _buffer.Put(c);
                            State = TokenizerState.Data;
                        }
                        break;
                    case TokenizerState.RCDATAEndTagOpen:
                        c = GetChar(out isBadChar);
                        if (IsAlpha(c))
                        {
                            currentTag = new Token.EndTag(c | 32);
                            temporaryBuffer.Add(c);
                            State = TokenizerState.RCDATAEndTagName;
                        }
                        else
                        {
                            yield return new Token.Character('<');
                            yield return new Token.Character('/');
                            _buffer.Put(c);
                            State = TokenizerState.Data;
                        }
                        break;
                    case TokenizerState.RCDATAEndTagName:
                        c = GetChar(out isBadChar);
                        if (IsSpace(c) && currentTag.TagName == LastStartTag.TagName)
                        {
                            State = TokenizerState.BeforeAttributeName;
                            break;
                        }
                        if (c == '/' && currentTag.TagName == LastStartTag.TagName)
                        {
                            State = TokenizerState.SelfClosingStartTag;
                            break;
                        }
                        if (c == '>' && currentTag.TagName == LastStartTag.TagName)
                        {
                            yield return currentTag;
                            currentTag = null;
                            temporaryBuffer = null;
                            State = TokenizerState.Data;
                            ContentModel = ContentModel.PcData;
                            break;
                        }
                        if (IsAlpha(c))
                        {
                            currentTag.AppendToName(c | 32);
                            temporaryBuffer.Add((char)c);
                            break;
                        }
                        yield return new Token.Character('<');
                        yield return new Token.Character('/');
                        foreach (var ch in temporaryBuffer)
                        {
                            yield return new Token.Character(ch);
                        }
                        temporaryBuffer = null;
                        _buffer.Put(c);
                        State = TokenizerState.Data;
                        break;
                    default:
                        throw new Exception("#1109 " + State);
                }
            }
        }

        private int GetChar(out bool isBadChar)
        {
            int c = _buffer.Get();
            if (c == EOF)
            {
                isBadChar = false;
                return EOF;
            }
            c = CheckChar(c, out isBadChar);
            return c;
        }

        private static bool IsSpace(int c)
        {
            return (c == ' ' || c == '\n' || c == '\f' || c == '\t');
        }

        private static bool IsUpper(int c)
        {
            return c >= 'A' && c <= 'Z';
        }

        private static bool IsLower(int c)
        {
            return c >= 'a' && c <= 'z';
        }

        private static bool IsAlpha(int c)
        {
            return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
        }

        /*
                private static bool IsDigit(int c)
                {
                    return (c >= '0' && c <= '9');
                }
        */

        /*
                private static bool IsHexDigit(int c)
                {
                    return (c >= '0' && c <= '9') || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f');
                }
        */

        /// <summary>
        /// Check that the next bytes match p. Consume these if so.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="ignoreCase"></param>
        /// <returns></returns>
        protected bool BufferMatch(string s, bool ignoreCase)
        {
            int len = s.Length;
            var buf = new int[len];
            for (int i = 0; i < len; i++)
            {
                int c = _buffer.Get();
                buf[i] = c;
                if (c != s[i])
                {
                    if (!(ignoreCase && ((IsUpper(c) && (c | 32) == s[i]) || (IsLower(c) && c - 32 == s[i]))))
                    {
                        for (int j = i; j >= 0; j--)
                            _buffer.Put(buf[j]);
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// If the last attribute have the same name as a previous one, remove it.
        /// </summary>
        /// <param name="attrs"></param>
        /// <returns>true si dup (parse error)</returns>
        private bool RemoveLastAttributeIfDup(List<Token.Tag.Attribute> attrs)
        {
            if (attrs == null)
                return false;
            int n = attrs.Count - 1;
            if (n <= 0)
                return false;
            Token.Tag.Attribute last = attrs[n];
            for (int i = 0; i < n; i++)
            {
                if (attrs[i].Name == last.Name)
                {
                    attrs.RemoveAt(n);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Check that the next few characters match the start tag (and are complete).
        /// </summary>
        /// <param name="startTag"></param>
        /// <returns></returns>
        protected bool MatchTagAndEnd(Token.StartTag startTag)
        {
            int len = startTag.TagNameBuilder.Length;
            var sb = new StringBuilder();
            int c = EOF;
            bool rc = true;
            for (int i = 0; i < len; i++)
            {
                c = _buffer.Get();
                if (c <= 32 || c > 127 || char.ToLowerInvariant((char)c) != startTag.TagNameBuilder[i])
                {
                    rc = false;
                    break;
                }
                sb.Append((char)c);
            }
            if (rc)
            {
                c = _buffer.Get();
                if (c != '\t' && c != '\n' && c != '\f' && c != ' ' && c != '>' && c != '/' && c != EOF)
                {
                    rc = false;
                }
            }
            _buffer.Put(c);
            _buffer.Put(sb.ToString());
            return rc;
        }

        /// <summary>
        /// Consume a character reference.
        /// </summary>
        /// <param name="c"></param>
        /// <param name="c2"></param>
        /// <param name="additionalCharacter"></param>
        /// <param name="inAttribute"></param>
        /// <param name="isParseError"> </param>
        /// <param name="isBadChar"> </param>
        /// <returns></returns>
        protected bool ConsumeCharRef(out int c, out int c2, char[] additionalCharacter, bool inAttribute, out bool isParseError, out bool isBadChar)
        {
            c2 = -1;
            isParseError = false;
            isBadChar = false;
            c = _buffer.Get();
            if (IsSpace(c) || c == '<' || c == '&'
                || c == EOF
                || (additionalCharacter.Length > 0 && c == additionalCharacter[0]))
            {
                _buffer.Put(c);
                return false;
            }
            if (c == '#')
            {
                bool hex = false;
                c = _buffer.Get();
                if (c == 'x' || c == 'X')
                {
                    hex = true;
                    c = _buffer.Get();
                }
                int cl;
                if (!ConsumeNumericCharRef(c, hex, out cl, out isParseError, out isBadChar))
                {
                    if (hex)
                        _buffer.Put('x');
                    _buffer.Put('#');
                    isParseError = true;
                    return false;
                }
                if (!isBadChar)
                    c = CheckChar(cl, out isBadChar);
                return true;
            }
            _buffer.Put(c);
            string s;
            if (!NamedCharRef.TryGetCharRef(_buffer, out s, out c, out c2))
            {
                //Buffer.Put(s);
                isParseError = true;
                return false;
            }
            if (!s.EndsWith(";"))
            {
                isParseError = true;
                if (inAttribute)
                {
                    c = _buffer.Get();
                    _buffer.Put(c);
                    if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
                    {
                        _buffer.Put(s);
                        return false;
                    }
                }
            }

            return true;
        }

        private bool ConsumeNumericCharRef(int c, bool hex, out int n, out bool isParseError, out bool isBadChar)
        {
            n = 0;
            int mult = hex ? 16 : 10;
            bool rc = false;
            isParseError = false;
            isBadChar = false;
            while (true)
            {
                unchecked
                {
                    if (c >= '0' && c <= '9')
                        n = n * mult + (c - '0');
                    else if (hex && ((c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f')))
                        n = n * 16 + 9 + (c & 7);
                    else break;
                    if (n > 0x10FFFF)
                        isBadChar = true;
                }
                c = _buffer.Get();
                rc = true;
            }
            if (c != ';')
            {
                _buffer.Put(c);
                isParseError = true;
            }
            if (isBadChar)
                n = 0xFFFD;
            return rc;
        }

        private int CheckChar(int c, out bool isBadChar)
        {
            int n;
            isBadChar = false;
            if (c > 0x10FFFF)
            {
                isBadChar = true;
                return 0xFFFD;
            }
            if (BadChar.TryGetValue(c, out n))
            {
                isBadChar = true;
                return n;
            }
            if ((c >= 0x0001 && c <= 0x0008) || (c >= 0x000E && c <= 0x001F)
                || (c >= 0x007F && c <= 0x009F) || (c >= 0xD800 && c <= 0xDFFF)
                || (c >= 0xFDD0 && c <= 0xFDEF))
                isBadChar = true;
            return c;
        }

        #endregion

        #region IEnumerable Membres

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}