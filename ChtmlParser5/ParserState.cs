﻿using System.Collections.Generic;
using HtmlParser5.Dom;
using HtmlParser5.Html;

namespace HtmlParser5
{
    public class ParserState
    {
        public List<Node> ActiveFormattingElements = new List<Node>();
        public bool Error;

        /// <summary>
        /// Last form seen.
        /// </summary>
        public Form Form;

        public bool FramesetOk = true;

        /// <summary>
        /// Head.
        /// </summary>
        public Head Head;

        public InsertionMode InsertionMode;
        public Stack<Node> OpenElements = new Stack<Node>();
        public InsertionMode OriginalInsertionMode;
        public InsertionMode SecondaryInsertionMode;

        private Html.Html _html;

        /// <summary>
        /// Html (first element of the stack).
        /// </summary>
        public Html.Html Html
        {
            get
            {
                if (_html == null)
                    foreach (Node n in OpenElements)
                        if (n is Html.Html) _html = (Html.Html)n;
                return _html;
            }
        }

        public Node CurrentNode
        {
            get
            {
                if (OpenElements.Count == 0)
                    return null;
                return OpenElements.Peek();
            }
        }

        public Node CurrentTable
        {
            get
            {
                int n = OpenElements.Count;
                int i = 0;
                foreach (Node e in OpenElements)
                {
                    if (e is Table)
                        return e;
                    // Stack bottom?
                    if (++i == n)
                        return e;
                }
                return null;
            }
        }

        /// <summary>
        /// Returns the 2nd stack element if it is a body element.
        /// </summary>
        /// <returns></returns>
        public Body GetBody()
        {
            Node[] nodes = OpenElements.ToArray();
            if (nodes.Length < 2 || !(nodes[1] is Body))
                return null;
            return (Body)nodes[1];
        }

        /// <summary>
        /// Check that a node type is in scope.
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public bool IsNodeInScope(Node target)
        {
            foreach (Node n in OpenElements)
            {
                if (target.GetType() == n.GetType())
                    return true;
                if (n is IScoping || n is Svg.Scoping)
                    return false;
            }
            // Can't reach
            return false;
        }

        public bool IsNodeInTableScope(Node target)
        {
            foreach (Node n in OpenElements)
            {
                if (target == n)
                    return true;
                if (n is Html.Html || n is Table)
                    return false;
            }
            // Can't reach
            return false;
        }

        public void ReconstructActiveFormattingElements()
        {
            if (ActiveFormattingElements.Count == 0)
                return;
            int pos = ActiveFormattingElements.Count - 1;
            Node entry = ActiveFormattingElements[pos];
            if (!(entry is IFormatting) || IsNodeInScope(entry))
                return;

            loop:
            if (pos == 0)
                goto create;
            pos--;
            entry = ActiveFormattingElements[pos];
            if (entry is IFormatting && !IsNodeInScope(entry))
                goto loop;

            loop2:
            pos++;
            entry = ActiveFormattingElements[pos];

            create:
            Node newNode = entry.CloneNode();
            CurrentNode.AppendChild(newNode);
            OpenElements.Push(newNode);
            ActiveFormattingElements[pos] = newNode;
            if (pos + 1 < ActiveFormattingElements.Count)
                goto loop2;
        }

        /// <summary>
        /// Clear active formatting elements up to a marker.
        /// </summary>
        public void ClearActiveFormattingElements()
        {
            if (ActiveFormattingElements.Count == 0)
                return;
            for (int pos = ActiveFormattingElements.Count - 1; pos >= 0; pos--)
            {
                Node entry = ActiveFormattingElements[pos];
                ActiveFormattingElements.RemoveAt(pos);
                if (!(entry is IFormatting))
                    break;
            }
        }

        /// <summary>
        /// Remove an active formatting element after the last marker.
        /// </summary>
        public bool RemoveActiveFormattingElement(Node target)
        {
            if (ActiveFormattingElements.Count == 0)
                return false;
            for (int pos = ActiveFormattingElements.Count - 1; pos >= 0; pos--)
            {
                Node entry = ActiveFormattingElements[pos];
                if (entry.GetType() == target.GetType())
                {
                    ActiveFormattingElements.RemoveAt(pos);
                    return true;
                }
                if (!(entry is IFormatting))
                    return false;
            }
            return false;
        }

        /// <summary>
        /// Search if a given node type is in the list after the last marker.
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public bool IsInActiveFormattingElements(Node target)
        {
            if (ActiveFormattingElements.Count == 0)
                return false;
            for (int pos = ActiveFormattingElements.Count - 1; pos >= 0; pos--)
            {
                Node n = ActiveFormattingElements[pos];
                if (n.GetType() == target.GetType())
                    return true;
                if (!(n is IFormatting))
                    break;
            }
            return false;
        }
    }
}