﻿using System.Text;

namespace HtmlParser5.Dom
{
    public class CharacterData : Node
    {
        public StringBuilder DataBuilder;
        private string _data;

        public CharacterData()
        {
            DataBuilder = new StringBuilder();
        }

        public CharacterData(string s)
        {
            Data = s;
        }

        public CharacterData(StringBuilder sb)
        {
            Data = sb.ToString();
        }

        public string Data
        {
            get
            {
                if (_data == null)
                {
                    if (DataBuilder != null)
                        _data = DataBuilder.ToString();
                    DataBuilder = null;
                }
                return _data;
            }
            private set { _data = value; }
        }
    }
}