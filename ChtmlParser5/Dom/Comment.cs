﻿namespace HtmlParser5.Dom
{
    public class Comment : CharacterData
    {
        public Comment(Token.Comment c)
            : base(c.Content)
        {
        }
    }
}