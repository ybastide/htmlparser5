﻿using System;
using System.Collections.Generic;

namespace HtmlParser5.Dom
{
    public class Node
    {
        public Dictionary<string, string> Attributes;

        private WeakReference _parent;

        protected Node(List<Token.Tag.Attribute> attrs)
        {
            SetAttributes(attrs);
        }

        protected Node()
        {
            Attributes = new Dictionary<string, string>();
        }

        public List<Node> ChildNodes { get; set; }

        public Node ParentNode
        {
            get
            {
                if (_parent == null)
                    return null;
                if (_parent.IsAlive)
                    return (Node)_parent.Target;
                return null;
            }
            set { _parent = new WeakReference(value); }
        }

        public Node FirstChild
        {
            get
            {
                if (ChildNodes == null || ChildNodes.Count == 0)
                    return null;
                return ChildNodes[0];
            }
        }

        public Node LastChild
        {
            get
            {
                if (ChildNodes == null || ChildNodes.Count == 0)
                    return null;
                return ChildNodes[ChildNodes.Count - 1];
            }
        }

        public Node SetAttributes(List<Token.Tag.Attribute> attrs)
        {
            Attributes = new Dictionary<string, string>();
            if (attrs == null)
                return this;
            foreach (Token.Tag.Attribute a in attrs)
                if (!Attributes.ContainsKey(a.Name))
                    Attributes.Add(a.Name, a.Value);
            return this;
        }

        public void AppendChild(Node newNode)
        {
            if (ChildNodes == null)
                ChildNodes = new List<Node>();

            ChildNodes.Add(newNode);
            newNode.ParentNode = this;
        }

        public virtual Node CloneNode()
        {
            var n = new Node {Attributes = Attributes};
            return n;
        }

        public Node RemoveChild(Node oldChild)
        {
            if (ChildNodes == null || ChildNodes.Count == 0)
                return null;
            if (!ChildNodes.Remove(oldChild))
                throw new NotFoundException();
            oldChild._parent = null;
            return oldChild;
        }
    }
}