﻿namespace HtmlParser5.Dom
{
    public class Text : CharacterData
    {
        public Text(char ch)
        {
            DataBuilder.Append(ch);
        }

        public void Append(char ch)
        {
            DataBuilder.Append(ch);
        }
    }
}
