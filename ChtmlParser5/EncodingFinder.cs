﻿using System;
using System.IO;
using System.Text;
using TswBase;

namespace HtmlParser5
{
    public static class EncodingFinder
    {
        private static readonly byte[] Spaces = new byte[] {0x09, 0x0A, 0x0C, 0x0D, 0x20};
        private static readonly byte[] SpacesAndSlash = new byte[] {0x09, 0x0A, 0x0C, 0x0D, 0x20, (byte)'/'};

        static EncodingFinder()
        {
        }

        /// <summary>
        /// Reads the stream and try detecting the encoding.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="defaultEncoding"></param>
        /// <returns></returns>
        public static Encoding SniffEncoding(Stream stream, Encoding defaultEncoding)
        {
            return SniffEncoding(new HtmlSimpleBuffer(stream), defaultEncoding);
        }

        /// <summary>
        /// Fills the buffer and try detecting the encoding.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="defaultEncoding"></param>
        /// <returns></returns>
        public static Encoding SniffEncoding(HtmlSimpleBuffer buffer, Encoding defaultEncoding)
        {
            buffer.Fill();
            if (buffer.Length == 0)
                return defaultEncoding;
            if (buffer[0] == 0xfe && buffer[1] == 0xff)
                return Encoding.GetEncoding(1201);
            if (buffer[0] == 0xff && buffer[1] == 0xfe)
                return Encoding.GetEncoding(1200);
            if (buffer[0] == 0xef && buffer[1] == 0xbb && buffer[2] == 0xbf)
                return Encoding.UTF8;

            int pos = 0;
            int c;
            while ((c = buffer[pos]) != HtmlSimpleBuffer.EOF)
            {
                pos++;
                if (c != '<')
                    continue;
                if (pos + 8 >= HtmlSimpleBuffer.BufferSize)
                    break; // nothing interesting
                if (buffer[pos] == '!' && buffer[pos + 1] == '-' && buffer[pos + 2] == '-')
                {
                    // Go after "-->", accepting "<-->".
                    while (buffer[pos] != HtmlSimpleBuffer.EOF)
                    {
                        pos++;
                        if (buffer[pos] == '-' && buffer[pos + 1] == '-' && buffer[pos + 2] == '>')
                            break;
                    }
                    pos += 3;
                    continue;
                }
                if (buffer.GetLower(pos) == 'm'
                    && buffer.GetLower(pos + 1) == 'e'
                    && buffer.GetLower(pos + 2) == 't'
                    && buffer.GetLower(pos + 3) == 'a'
                    && Array.IndexOf(SpacesAndSlash, (byte)buffer[pos + 4]) >= 0
                    )
                {
                    pos += 4;
                    while (true)
                    {
                        string name, value;
                        GetAttr(buffer, ref pos, out name, out value);
                        if (string.IsNullOrEmpty(name))
                            break;
                        if (name == "charset")
                        {
                            Encoding e = SafeEncoding(value, null);
                            if (e != null)
                                return e;
                        }
                        else if (name == "content")
                        {
                            Encoding e = GetEncodingFromContentType(new SubString(value), null);
                            if (e != null)
                                return e;
                        }
                    }
                }
            }

            return defaultEncoding;
        }

        /// <summary>
        /// Primitive GetAttribute on a byte array.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="pos"></param>
        /// <param name="name"></param>
        /// <param name="value"></param>
        private static void GetAttr(HtmlSimpleBuffer buffer, ref int pos, out string name, out string value)
        {
            name = null;
            value = null;
            int c;
            do
            {
                c = buffer[pos++];
            } while (Array.IndexOf(SpacesAndSlash, (byte)c) >= 0);

            if (c == '>' || c == HtmlSimpleBuffer.EOF)
                return;

            var bName = new byte[16];
            int nameLen = 0;
            while (c != HtmlSimpleBuffer.EOF)
            {
                if (c == '=' /*&& name_len > 0*/)
                    break;
                if (Array.IndexOf(Spaces, (byte)c) >= 0)
                    break;
                if (c == '/' || c == '>')
                {
                    name = Encoding.ASCII.GetString(bName, 0, nameLen);
                    return;
                }
                if (nameLen < bName.Length)
                    bName[nameLen++] = (byte)c;
                c = buffer.GetLower(pos++);
            }
            if (nameLen == bName.Length)
                return;
            name = Encoding.ASCII.GetString(bName, 0, nameLen);

            while (Array.IndexOf(Spaces, (byte)c) >= 0)
                c = buffer[pos++];
            if (c != '=')
                return;
            c = buffer[pos++];
            while (Array.IndexOf(Spaces, (byte)c) >= 0)
                c = buffer[pos++];

            var bValue = new byte[64];
            int valueLen = 0;

            if (c == '"' || c == '\'')
            {
                int b = c;
                while ((c = buffer.GetLower(pos++)) != b && c != HtmlSimpleBuffer.EOF)
                    if (valueLen < bValue.Length)
                        bValue[valueLen++] = (byte)c;
                if (c == b && valueLen < bValue.Length)
                    value = Encoding.ASCII.GetString(bValue, 0, valueLen);
                return;
            }
            while (true)
            {
                if (c == HtmlSimpleBuffer.EOF || c == '>' || Array.IndexOf(Spaces, (byte)c) >= 0)
                    break;
                if (valueLen < bValue.Length)
                    bValue[valueLen++] = (byte)c;
                c = buffer.GetLower(pos++);
            }
            value = Encoding.ASCII.GetString(bValue, 0, valueLen);
        }

        private static Encoding SafeEncoding(string name, Encoding defaultEncoding)
        {
            if (string.IsNullOrEmpty(name))
                return defaultEncoding;
            if (name == "utf8")
                return Encoding.UTF8;
            if (name == "iso-8859-1")
                return Encoding.GetEncoding(1252);
            try
            {
                return Encoding.GetEncoding(name);
            }
            catch (ArgumentException)
            {
                return defaultEncoding;
            }
        }

        public static Encoding GetEncodingFromContentType(SubString contentType, Encoding defaultEncoding)
        {
            if (SubString.IsNullOrEmpty(contentType))
                return defaultEncoding;
            int len = contentType.Length;
            // Search "charset"
            int p = contentType.IndexOf("charset", StringComparison.OrdinalIgnoreCase);
            if (p < 0)
                return defaultEncoding;
            p += "charset".Length;
            // skip spaces
            while (p < len && contentType[p] < 256 && Array.IndexOf(Spaces, (byte)contentType[p]) >= 0)
                p++;
            // check for "="
            if (p == len || contentType[p] != '=')
                return defaultEncoding;
            p++;
            // skip spaces
            while (p < len && contentType[p] < 256 && Array.IndexOf(Spaces, (byte)contentType[p]) >= 0)
                p++;
            if (p == len)
                return defaultEncoding;
            var sb = new StringBuilder();
            // quoted charset
            if (contentType[p] == '\'' || contentType[p] == '"')
            {
                char b = contentType[p];
                p++;
                if (p == len)
                    return defaultEncoding;
                do
                {
                    char c = contentType[p];
                    if (c == b)
                        return SafeEncoding(sb.ToString(), defaultEncoding);
                    sb.Append(c);
                    p++;
                } while (p < len);
                return defaultEncoding;
            }
            // unquoted charset
            while (true)
            {
                if (p == len)
                    break;
                char c = contentType[p];
                if (Array.IndexOf(Spaces, (byte)contentType[p]) >= 0)
                    break;
                sb.Append(c);
                p++;
            }

            return SafeEncoding(sb.ToString(), defaultEncoding);
        }
    }
}