﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using HtmlParser5;
using NUnit.Framework;

namespace tests
{
    [TestFixture]
    public class TestHtmlCookedBuffer
    {
        private static HtmlCookedBuffer CreateBuffer(string content)
        {
            return CreateBuffer(content, Encoding.UTF8);
        }

        private static HtmlCookedBuffer CreateBuffer(string content, Encoding encoding)
        {
            byte[] buf = encoding.GetBytes(content);
            MemoryStream ms = new MemoryStream(buf);
            return new HtmlCookedBuffer(ms, encoding);
        }

        [Test]
        public void t_00_empty()
        {
            HtmlCookedBuffer buffer = CreateBuffer("");
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_01_abc()
        {
            HtmlCookedBuffer buffer = CreateBuffer("abc");
            Assert.AreEqual('a', buffer.Get());
            Assert.AreEqual('b', buffer.Get());
            Assert.AreEqual('c', buffer.Get());
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_02_utf32()
        {
            MemoryStream ms = new MemoryStream(new byte[] { 0xF0, 0xA4, 0xAD, 0xA2 });
            HtmlCookedBuffer buffer = new HtmlCookedBuffer(ms, Encoding.UTF8);
            Assert.AreEqual(0xd852, buffer.Get());
            Assert.AreEqual(0xdf62, buffer.Get());
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_03_cr()
        {
            HtmlCookedBuffer buffer = CreateBuffer("\r");
            Assert.AreEqual('\n', buffer.Get());
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_04_crlf()
        {
            HtmlCookedBuffer buffer = CreateBuffer("\r\n");
            Assert.AreEqual('\n', buffer.Get());
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_05_put_01()
        {
            HtmlCookedBuffer buffer = CreateBuffer("");
            buffer.Put('a');
            Assert.AreEqual('a', buffer.Get());
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_05_put_02()
        {
            HtmlCookedBuffer buffer = CreateBuffer("");
            buffer.Put('a');
            buffer.Put('b');
            Assert.AreEqual('b', buffer.Get());
            Assert.AreEqual('a', buffer.Get());
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_05_put_03()
        {
            HtmlCookedBuffer buffer = CreateBuffer("abc");
            Assert.AreEqual('a', buffer.Get());
            Assert.AreEqual('b', buffer.Get());
            buffer.Put('b');
            buffer.Put('a');
            Assert.AreEqual('a', buffer.Get());
            Assert.AreEqual('b', buffer.Get());
            Assert.AreEqual('c', buffer.Get());
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_06_endswith_01()
        {
            HtmlCookedBuffer buffer = CreateBuffer("abc");
            buffer.Get();
            buffer.Get();
            buffer.Get();
            Assert.IsTrue(buffer.EndsWith("abc"));
        }

        [Test]
        public void t_06_endswith_02()
        {
            HtmlCookedBuffer buffer = CreateBuffer("abc");
            buffer.Get();
            buffer.Get();
            buffer.Get();
            Assert.IsFalse(buffer.EndsWith("abd"));
        }

        [Test]
        public void t_06_endswith_03()
        {
            HtmlCookedBuffer buffer = CreateBuffer("abc\r");
            buffer.Get();
            buffer.Get();
            buffer.Get();
            buffer.Get();
            Assert.IsTrue(buffer.EndsWith("abc\n"));
        }

        [Test]
        public void t_06_endswith_04()
        {
            HtmlCookedBuffer buffer = CreateBuffer("abc\r\n");
            buffer.Get();
            buffer.Get();
            buffer.Get();
            buffer.Get();
            Assert.IsTrue(buffer.EndsWith("abc\n"));
        }

        [Test]
        public void t_06_endswith_05()
        {
            HtmlCookedBuffer buffer = CreateBuffer("ab");
            buffer.Get();
            buffer.Get();
            buffer.Put('c');
            Assert.IsTrue(buffer.EndsWith("abc"));
        }

        [Test]
        public void t_06_endswith_06()
        {
            HtmlCookedBuffer buffer = CreateBuffer("ab");
            buffer.Get();
            buffer.Get();
            Assert.IsFalse(buffer.EndsWith("abc"));
        }

#if false
        /// <summary>
        /// XXX???
        /// </summary>
        [Test]
        public void t_06_endswith_07()
        {
            HtmlCookedBuffer buffer = CreateBuffer("ab");
            buffer.Get();
            buffer.Get();
            buffer.Put('c');
            buffer.Put('b');
            Assert.IsTrue(buffer.EndsWith("abc"));
        }
#endif

        [Test]
        public void t_07_puts_01()
        {
            HtmlCookedBuffer buffer = CreateBuffer("");
            buffer.Put("abc");
            Assert.AreEqual('a', buffer.Get());
            Assert.AreEqual('b', buffer.Get());
            Assert.AreEqual('c', buffer.Get());
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_07_puts_02()
        {
            HtmlCookedBuffer buffer = CreateBuffer("");
            buffer.Put("abc");
            Assert.IsTrue(buffer.EndsWith("abc"));
        }
    }
}
