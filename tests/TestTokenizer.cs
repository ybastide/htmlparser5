﻿using System;
using System.IO;
using System.Text;
using HtmlParser5;
using NUnit.Framework;

namespace tests
{
    internal class MyTokenizer : Tokenizer
    {
        public MyTokenizer(HtmlCookedBuffer buffer)
            : base(buffer)
        {
        }

        public MyTokenizer(string s)
            : base(new MemoryStream(Encoding.UTF8.GetBytes(s)), Encoding.UTF8, null)
        {
        }

        public bool ConsumeCharRef(out int c, out int c2)
        {
            return ConsumeCharRef(out c, out c2, new char[0], false);
        }

        public bool ConsumeCharRef(out int c, out int c2, char[] additionalCharacter, bool inAttribute)
        {
            bool ispe;
            bool isBadChar;
            return ConsumeCharRef(out c, out c2, additionalCharacter, inAttribute, out ispe, out isBadChar);
        }

        public new bool MatchTagAndEnd(Token.StartTag startTag)
        {
            return base.MatchTagAndEnd(startTag);
        }

        public new bool BufferMatch(string s, bool ignoreCase)
        {
            return base.BufferMatch(s, ignoreCase);
        }

        public bool ConsumeCharRef(out int c)
        {
            int c2;
            bool rc = ConsumeCharRef(out c, out c2);
            if (c2 != -1)
                throw new Exception("#46");
            return rc;
        }

        public bool ConsumeCharRef(out int c, char[] additionalCharacter, bool inAttribute)
        {
            int c2;
            bool rc = ConsumeCharRef(out c, out c2, additionalCharacter, inAttribute);
            if (c2 != -1)
                throw new Exception("#46");
            return rc;
        }
    }

    [TestFixture]
    public class TestTokenizer
    {
        private static HtmlCookedBuffer CreateBuffer(string content)
        {
            return CreateBuffer(content, Encoding.UTF8);
        }

        private static HtmlCookedBuffer CreateBuffer(string content, Encoding encoding)
        {
            byte[] buf = encoding.GetBytes(content);
            var ms = new MemoryStream(buf);
            return new HtmlCookedBuffer(ms, encoding);
        }

        // ReSharper disable InconsistentNaming
        [Test]
        public void t_01_num_char_ref_01()
        {
            int c;
            var tok = new MyTokenizer("#97;");
            int c2;
            Assert.IsTrue(tok.ConsumeCharRef(out c, out c2));
            Assert.AreEqual('a', c);
            Assert.AreEqual(-1, c2);
            //Assert.IsFalse(tok.ParseError);
        }

        [Test]
        public void t_01_num_char_ref_02()
        {
            int c;
            var tok = new MyTokenizer("#x20Ac");
            Assert.IsTrue(tok.ConsumeCharRef(out c));
            Assert.AreEqual('€', (char)c);
            //Assert.IsTrue(tok.ParseError);
        }

        [Test]
        public void t_02_named_char_ref_01()
        {
            int c;
            var tok = new MyTokenizer("agrave;");
            Assert.IsTrue(tok.ConsumeCharRef(out c));
            Assert.AreEqual('à', c);
            //Assert.IsFalse(tok.ParseError);
        }

        [Test]
        public void t_02_named_char_ref_02()
        {
            int c;
            HtmlCookedBuffer buffer = CreateBuffer("amp");
            var tok = new MyTokenizer(buffer);
            Assert.IsTrue(tok.ConsumeCharRef(out c));
            Assert.AreEqual('&', c);
            //Assert.IsTrue(tok.ParseError);
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_02_named_char_ref_03()
        {
            int c;
            HtmlCookedBuffer buffer = CreateBuffer("ampere");
            var tok = new MyTokenizer(buffer);
            Assert.IsTrue(tok.ConsumeCharRef(out c));
            Assert.AreEqual('&', c);
            //Assert.IsTrue(tok.ParseError);
            Assert.AreEqual('e', (char)buffer.Get());
            Assert.AreEqual('r', (char)buffer.Get());
            Assert.AreEqual('e', (char)buffer.Get());
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_02_named_char_ref_04()
        {
            int c;
            HtmlCookedBuffer buffer = CreateBuffer("ampere");
            var tok = new MyTokenizer(buffer);
            Assert.IsFalse(tok.ConsumeCharRef(out c, new char[0], true));
            //Assert.IsTrue(tok.ParseError);
            Assert.AreEqual('a', (char)buffer.Get());
            Assert.AreEqual('m', (char)buffer.Get());
            Assert.AreEqual('p', (char)buffer.Get());
            Assert.AreEqual('e', (char)buffer.Get());
            Assert.AreEqual('r', (char)buffer.Get());
            Assert.AreEqual('e', (char)buffer.Get());
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_03_MatchTagAndEnd_01()
        {
            HtmlCookedBuffer buffer = CreateBuffer("Em> ");
            var tok = new MyTokenizer(buffer);
            var start = new Token.StartTag("em");
            Assert.IsTrue(tok.MatchTagAndEnd(start));
            Assert.AreEqual('E', (char)buffer.Get());
            Assert.AreEqual('m', (char)buffer.Get());
            Assert.AreEqual('>', (char)buffer.Get());
            Assert.AreEqual(' ', (char)buffer.Get());
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_03_MatchTagAndEnd_02()
        {
            HtmlCookedBuffer buffer = CreateBuffer("em >");
            var tok = new MyTokenizer(buffer);
            var start = new Token.StartTag("em");
            Assert.IsTrue(tok.MatchTagAndEnd(start));
            Assert.AreEqual('e', (char)buffer.Get());
            Assert.AreEqual('m', (char)buffer.Get());
            Assert.AreEqual(' ', (char)buffer.Get());
            Assert.AreEqual('>', (char)buffer.Get());
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_03_MatchTagAndEnd_03()
        {
            HtmlCookedBuffer buffer = CreateBuffer("em >");
            var tok = new MyTokenizer(buffer);
            var start = new Token.StartTag("a");
            Assert.IsFalse(tok.MatchTagAndEnd(start));
            Assert.AreEqual('e', (char)buffer.Get());
            Assert.AreEqual('m', (char)buffer.Get());
            Assert.AreEqual(' ', (char)buffer.Get());
            Assert.AreEqual('>', (char)buffer.Get());
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_03_MatchTagAndEnd_04()
        {
            HtmlCookedBuffer buffer = CreateBuffer("eme>");
            var tok = new MyTokenizer(buffer);
            var start = new Token.StartTag("em");
            Assert.IsFalse(tok.MatchTagAndEnd(start));
            Assert.AreEqual('e', (char)buffer.Get());
            Assert.AreEqual('m', (char)buffer.Get());
            Assert.AreEqual('e', (char)buffer.Get());
            Assert.AreEqual('>', (char)buffer.Get());
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_03_MatchTagAndEnd_05()
        {
            HtmlCookedBuffer buffer = CreateBuffer("em>");
            var tok = new MyTokenizer(buffer);
            var start = new Token.StartTag("eme");
            Assert.IsFalse(tok.MatchTagAndEnd(start));
            Assert.AreEqual('e', (char)buffer.Get());
            Assert.AreEqual('m', (char)buffer.Get());
            Assert.AreEqual('>', (char)buffer.Get());
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_04_BufferMatch_01()
        {
            HtmlCookedBuffer buffer = CreateBuffer("doctype");
            var tok = new MyTokenizer(buffer);
            Assert.IsTrue(tok.BufferMatch("doctype", false));
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_04_BufferMatch_02()
        {
            HtmlCookedBuffer buffer = CreateBuffer("doctype");
            var tok = new MyTokenizer(buffer);
            Assert.IsFalse(tok.BufferMatch("docType", false));
            Assert.AreEqual('d', (char)buffer.Get());
            Assert.AreEqual('o', (char)buffer.Get());
            Assert.AreEqual('c', (char)buffer.Get());
            Assert.AreEqual('t', (char)buffer.Get());
            Assert.AreEqual('y', (char)buffer.Get());
            Assert.AreEqual('p', (char)buffer.Get());
            Assert.AreEqual('e', (char)buffer.Get());
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_04_BufferMatch_03()
        {
            HtmlCookedBuffer buffer = CreateBuffer("doctype");
            var tok = new MyTokenizer(buffer);
            Assert.IsTrue(tok.BufferMatch("DOCTYPE", true));
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_04_BufferMatch_04()
        {
            HtmlCookedBuffer buffer = CreateBuffer("DOCTYPE");
            var tok = new MyTokenizer(buffer);
            Assert.IsTrue(tok.BufferMatch("doctype", true));
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_04_BufferMatch_05()
        {
            HtmlCookedBuffer buffer = CreateBuffer("DOCTYP");
            var tok = new MyTokenizer(buffer);
            Assert.IsFalse(tok.BufferMatch("doctype", true));
            Assert.AreEqual('D', (char)buffer.Get());
            Assert.AreEqual('O', (char)buffer.Get());
            Assert.AreEqual('C', (char)buffer.Get());
            Assert.AreEqual('T', (char)buffer.Get());
            Assert.AreEqual('Y', (char)buffer.Get());
            Assert.AreEqual('P', (char)buffer.Get());
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_04_BufferMatch_06()
        {
            HtmlCookedBuffer buffer = CreateBuffer("DOCTYPE");
            var tok = new MyTokenizer(buffer);
            Assert.IsTrue(tok.BufferMatch("doctyp", true));
            Assert.AreEqual('E', (char)buffer.Get());
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_05_enum_01()
        {
            var buffer = CreateBuffer("<html>\r\n  <!--to--to--><tag a=b></tag><x y=\"1'\" /></html>");
            var tok = new MyTokenizer(buffer);
            int i = 0;
            foreach (var t in tok)
            {
                switch (i)
                {
                    case 0:
                        Assert.AreEqual(typeof(Token.StartTag), t.GetType());
                        Assert.AreEqual("html", ((Token.StartTag)t).TagName);
                        break;
                    case 1:
                        Assert.AreEqual(typeof(Token.Character), t.GetType());
                        Assert.AreEqual('\n', ((Token.Character)t).Ch);
                        break;
                    case 2:
                    case 3:
                        Assert.AreEqual(typeof(Token.Character), t.GetType());
                        Assert.AreEqual(' ', ((Token.Character)t).Ch);
                        break;
                    case 4:
                        Assert.AreEqual(typeof(Token.Comment), t.GetType());
                        break;
                    case 5:
                        Assert.AreEqual(typeof(Token.StartTag), t.GetType());
                        Assert.AreEqual("tag", ((Token.StartTag)t).TagName);
                        Assert.AreEqual(1, ((Token.StartTag)t).Attributes.Count);
                        Assert.AreEqual("a", ((Token.StartTag)t).Attributes[0].Name);
                        Assert.AreEqual("b", ((Token.StartTag)t).Attributes[0].Value);
                        break;
                    case 6:
                        Assert.AreEqual(typeof(Token.EndTag), t.GetType());
                        Assert.AreEqual("tag", ((Token.EndTag)t).TagName);
                        break;
                    case 7:
                        Assert.AreEqual(typeof(Token.StartTag), t.GetType());
                        Assert.AreEqual("x", ((Token.StartTag)t).TagName);
                        Assert.AreEqual(true, ((Token.StartTag)t).SelfClosing);
                        Assert.AreEqual(1, ((Token.StartTag)t).Attributes.Count);
                        Assert.AreEqual("y", ((Token.StartTag)t).Attributes[0].Name);
                        Assert.AreEqual("1'", ((Token.StartTag)t).Attributes[0].Value);
                        break;
                    case 8:
                        Assert.AreEqual(typeof(Token.EndTag), t.GetType());
                        Assert.AreEqual("html", ((Token.EndTag)t).TagName);
                        break;
                    case 9:
                        Assert.AreEqual(typeof(Token.EOF), t.GetType());
                        break;
                    default:
                        Assert.IsFalse(true);
                        break;
                }
                i++;
            }
        }

        [Test]
        public void t_06_a()
        {
            var buffer =
                CreateBuffer(
                    "<a id=\"tumblr_share\" href=\"http://www.tumblr.com/share/photo?&click_thru=http%3A%2F%2Ffrance.hermes.com%2Floisirs%2Fequitation%2Fpour-le-cheval%2Fconfigurable-product-800017e-27052.html%3Fnuance%3D1&source=http%3A%2F%2Fmedia.hermes.com%2Fmedia%2Fcatalog%2Fproduct%2Fimport%2FE%2FE88%2FE881%2Fitem%2Fdefault%2F800017E01.jpg&caption=%3Cb%3EBrosse+Douce%3C%2Fb%3E%3Cbr+%2F%3EBrosse+douce+de+pansage+en+h%C3%AAtre+et+soies+de+porc%2C+poign%C3%A9e+en+ch%C3%AAvre+aniline%2C+marquage+%22Herm%C3%A8s+Sellier%27%27+sous+la+poign%C3%A9e+%2819+x+8%2C5+x+4+cm%29\"\">");
            var tok = new MyTokenizer(buffer);
            int i = 0;
            foreach (var t in tok)
            {
                switch (i)
                {
                    case 0:
                        var a = t as Token.StartTag;
                        Assert.IsNotNull(a);
                        Assert.AreEqual("a", a.TagName);
                        Assert.AreEqual(3, a.Attributes.Count);
                        break;
                    case 1:
                        var eof = t as Token.EOF;
                        Assert.IsNotNull(eof);
                        break;
                }
                i++;
            }
        }

        // ReSharper restore InconsistentNaming
    }
}