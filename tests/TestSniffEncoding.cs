﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using HtmlParser5;
using NUnit.Framework;

namespace tests
{
    [TestFixture]
    public class TestSniffEncoding
    {
        Stream GetStream(string s)
        {
            return GetStream(s, Encoding.UTF8);
        }

        private Stream GetStream(string s, Encoding encoding)
        {
            if (s == null)
                return null;
            return new MemoryStream(encoding.GetBytes(s));
        }

        [Test]
        public void t_null_stream()
        {
            Stream s = GetStream(null);
            Assert.AreEqual(null, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_empty_stream()
        {
            Stream s = GetStream("");
            Assert.AreEqual(null, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_preamble_utf8()
        {
            Stream s = new MemoryStream(new UTF8Encoding(true).GetPreamble());
            Assert.AreEqual(Encoding.UTF8, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_preamble_utf16le()
        {
            Stream s = new MemoryStream(Encoding.GetEncoding(1200).GetPreamble());
            Assert.AreEqual(Encoding.Unicode, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_preamble_utf16be()
        {
            Stream s = new MemoryStream(Encoding.GetEncoding(1201).GetPreamble());
            Assert.AreEqual(Encoding.BigEndianUnicode, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_empty_comment()
        {
            Stream s = GetStream("<!-->");
            Assert.AreEqual(null, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_charset_01()
        {
            Stream s = GetStream(" <meta charset=utf8>");
            Assert.AreEqual(Encoding.UTF8, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_charset_02()
        {
            Stream s = GetStream("<MeTa cHArsEt=uTf8>");
            Assert.AreEqual(Encoding.UTF8, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_charset_03()
        {
            Stream s = GetStream("<meta charset=utf8\r\n\t>");
            Assert.AreEqual(Encoding.UTF8, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_charset_04()
        {
            Stream s = GetStream("<meta  charset=utf8>");
            Assert.AreEqual(Encoding.UTF8, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_charset_05()
        {
            Stream s = GetStream("<meta charset =utf8>");
            Assert.AreEqual(Encoding.UTF8, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_charset_06()
        {
            Stream s = GetStream("<meta charset= utf8>");
            Assert.AreEqual(Encoding.UTF8, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_charset_07()
        {
            Stream s = GetStream("<meta charset=utf8 >");
            Assert.AreEqual(Encoding.UTF8, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_charset_08()
        {
            Stream s = GetStream("<meta  charset      =       utf8        >");
            Assert.AreEqual(Encoding.UTF8, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_charset_09()
        {
            Stream s = GetStream("<meta charset=\"utf8\">");
            Assert.AreEqual(Encoding.UTF8, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_charset_10()
        {
            Stream s = GetStream("<meta charset=\"utf8\" >");
            Assert.AreEqual(Encoding.UTF8, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_charset_11()
        {
            Stream s = GetStream("<meta charset=\"utf8\"/>");
            Assert.AreEqual(Encoding.UTF8, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_charset_12()
        {
            Stream s = GetStream("<meta charset='utf8'>");
            Assert.AreEqual(Encoding.UTF8, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_charset_13()
        {
            Stream s = GetStream("<meta charset='utf8' >");
            Assert.AreEqual(Encoding.UTF8, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_charset_14()
        {
            Stream s = GetStream("<meta charset=\"utf8'/>");
            Assert.AreEqual(null, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_charset_15()
        {
            Stream s = GetStream(@"
<html>
  <!-- encoding-->
  <meta charset=utf8>");
            Assert.AreEqual(Encoding.UTF8, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_charset_commented()
        {
            Stream s = GetStream("<!--<meta charset=utf8>");
            Assert.AreEqual(null, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_content_type_01()
        {
            Stream s = GetStream("<meta http-equiv=content-type content=charset=utf8>");
            Assert.AreEqual(Encoding.UTF8, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_content_type_02()
        {
            Stream s = GetStream(@"
<meta http-equiv=refresh content=""5;url=index.html"">
<meta http-equiv=content-type content=charset=utf8>
");
            Assert.AreEqual(Encoding.UTF8, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_content_type_03()
        {
            Stream s = GetStream(@"
<meta http-equiv=content-type content=charset=utf8>
<meta http-equiv=refresh content=""5;url=index.html"">
");
            Assert.AreEqual(Encoding.UTF8, EncodingFinder.SniffEncoding(s, null));
        }

        [Test]
        public void t_content_type_04()
        {
            Stream s = GetStream(@"
<meta http-equiv=contewwwwwwwwwwwwwwwwwwwwwwwwwwnt-type content=charset=utf8>
<meta httxxxxxxxxxxxxxxxxxp-equiv=refresh content=""5;url=index.html"">
");
            Assert.AreEqual(Encoding.UTF8, EncodingFinder.SniffEncoding(s, null));
        }
    }

}
