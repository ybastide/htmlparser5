﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using HtmlParser5;
using NUnit.Framework;

namespace tests
{
    [TestFixture]
    public class TestSimpleBuffer
    {
        [Test]
        public void t01()
        {
            MemoryStream ms = new MemoryStream(new byte[0]);
            HtmlSimpleBuffer b = new HtmlSimpleBuffer(ms);
            b.Fill();
            Assert.AreEqual(0, b.Length);
            Assert.AreEqual(HtmlSimpleBuffer.EOF, b[0]);
            Assert.AreEqual(HtmlSimpleBuffer.EOF, b[int.MinValue]);
            Assert.AreEqual(HtmlSimpleBuffer.EOF, b[int.MaxValue]);
        }

        [Test]
        public void t02()
        {
            MemoryStream ms = new MemoryStream(new byte[] { 0x41, 0x61 });
            HtmlSimpleBuffer b = new HtmlSimpleBuffer(ms);
            b.Fill();
            Assert.AreEqual(2, b.Length);
            Assert.AreEqual('A', b[0]);
            Assert.AreEqual('a', b[1]);
            Assert.AreEqual(HtmlSimpleBuffer.EOF, b[2]);
        }

        [Test]
        public void t03()
        {
            MemoryStream ms = new MemoryStream(new byte[] { 0x41, 0x61 });
            HtmlSimpleBuffer b = new HtmlSimpleBuffer(ms);
            b.Fill();
            Assert.AreEqual(2, b.Length);
            Assert.AreEqual('a', b.GetLower(0));
            Assert.AreEqual('a', b.GetLower(1));
            Assert.AreEqual(HtmlSimpleBuffer.EOF, b.GetLower(2));
        }

        [Test]
        public void t04()
        {
            MemoryStream ms = null;
            HtmlSimpleBuffer b = new HtmlSimpleBuffer(ms);
            b.Fill();
            Assert.AreEqual(0, b.Length);
            Assert.AreEqual(HtmlSimpleBuffer.EOF, b[0]);
            Assert.AreEqual(HtmlSimpleBuffer.EOF, b[int.MinValue]);
            Assert.AreEqual(HtmlSimpleBuffer.EOF, b[int.MaxValue]);
        }

    }
}
