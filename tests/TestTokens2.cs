﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using NUnit.Framework;
using Newtonsoft.Json;
using HtmlParser5;
using Newtonsoft.Json.Linq;

namespace tests
{
    class MyTokenizer2 : Tokenizer
    {

        public MyTokenizer2(string input, string state, Token.StartTag lastStartTag)
            : base(
            new MemoryStream(MyGetBytes(input)), Encoding.Unicode, null)
        //new MemoryStream(new UTF32Encoding(false, false, true).GetBytes(input)), Encoding.UTF32, null)
        {
            SetState(state);
            LastStartTag = lastStartTag;
        }

        private static byte[] MyGetBytes(string input)
        {
            var res = new byte[input.Length * 2];
            int i = 0;
            foreach (var c in input)
            {
                res[i] = (byte)(c & 255);
                res[i + 1] = (byte)((c >> 8) & 255);
                i += 2;
            }
            return res;
        }

        void SetState(string state)
        {
            switch (state)
            {
                case "PLAINTEXT state":
                    ContentModel = ContentModel.PlainText;
                    break;
                case "PCDATA state":
                    ContentModel = ContentModel.PcData;
                    break;
                case "RCDATA state":
                    ContentModel = ContentModel.RcData;
                    break;
                case "RAWTEXT state":
                    ContentModel = ContentModel.RawText;
                    break;
                default:
                    throw new Exception(state);
            }
        }

        public List<object> Parse()
        {
            var res = new List<object>();
            StringBuilder sb = null;
            foreach (var token in this)
            {
                var ct = token as Token.Character;
                if (ct != null)
                {
                    if (sb == null)
                        sb = new StringBuilder();
                    sb.Append(ct.Ch);
                    if (ct.Surrogate != 0)
                        sb.Append(ct.Surrogate);
                }
                else
                {
                    if (sb != null)
                        res.Add(new List<string> { "Character", sb.ToString() });
                    sb = null;
                    if (token is Token.EOF)
                        break;
                    res.Add(token);
                }
            }
            return res;
        }
    }

#if false
    internal class MyEncoderFallback : EncoderFallback
    {
        public override EncoderFallbackBuffer CreateFallbackBuffer()
        {
            return new MyFallbackBuffer();
        }

        public override int MaxCharCount
        {
            get
            {
                throw new NotImplementedException();
            }
        }
    }

    internal class MyFallbackBuffer : EncoderFallbackBuffer
    {
        private char _charUnknown;
        public override bool Fallback(char charUnknown, int index)
        {
            _charUnknown=charUnknown;
            return true;
        }

        public override bool Fallback(char charUnknownHigh, char charUnknownLow, int index)
        {
            throw new NotImplementedException();
        }

        public override char GetNextChar()
        {
            return _charUnknown;
        }

        public override bool MovePrevious()
        {
            throw new NotImplementedException();
        }

        public override int Remaining
        {
            get
            {
                throw new NotImplementedException();
            }
        }
    }

    /// <summary>
    /// Décodeur UTF-8.
    /// </summary>
    public class MyDecoderFallback : DecoderFallback
    {
        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="MyDecoderFallbackBuffer"/>.
        /// </summary>
        /// <returns></returns>
        public override DecoderFallbackBuffer CreateFallbackBuffer()
        {
            return new MyDecoderFallbackBuffer();
        }

        /// <summary>
        /// Nombre max de caractères renvoyés.
        /// </summary>
        public override int MaxCharCount
        {
            get
            {
                return 1;
            }
        }
    }

    public class MyDecoderFallbackBuffer : DecoderFallbackBuffer
    {

        /// <summary>
        /// Octets à transformer.
        /// </summary>
        byte[] _bytesUnknown;

        /// <summary>
        /// Position dans <see cref="_bytesUnknown"/>.
        /// </summary>
        int _position;

        /// <summary>
        /// Peut-on traiter cette séquence ? Oui
        /// </summary>
        /// <param name="bytesUnknown"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public override bool Fallback(byte[] bytesUnknown, int index)
        {
            _bytesUnknown = bytesUnknown;
            _position = 0;
            return true;
        }

        /// <summary>
        /// Convertit un caractère, incrémente la position.
        /// </summary>
        /// <returns></returns>
        public override char GetNextChar()
        {
            if (_position >= _bytesUnknown.Length)
                return char.MinValue;
            int index = _bytesUnknown[_position++] - 128;
            Console.Error.WriteLine("*** GetNextChar: char = {0} ***", index);
            return char.MinValue;
        }

        /// <summary>
        /// Caractère précédent.
        /// </summary>
        /// <returns></returns>
        public override bool MovePrevious()
        {
            if (_position == 0)
                return false;
            _position--;
            return true;
        }

        /// <summary>
        /// Revient au début.
        /// </summary>
        public override void Reset()
        {
            _position = 0;
        }

        /// <summary>
        /// Octets restant à convertir.
        /// </summary>
        public override int Remaining
        {
            get
            {
                return _bytesUnknown.Length - _position;
            }
        }
    }
#endif

    [TestFixture]
    public class TestTokens2
    {
        [Test]
        public void T1()
        {
            foreach (var fname in Directory.GetFiles("testdata/tokenizer", "*.test"))
            {
                using (var reader = new JsonTextReader(new StreamReader(fname, Encoding.UTF8)))
                {
                    var serializer = new JsonSerializer();
                    var o = (JObject)serializer.Deserialize(reader);
                    JToken tok;
                    if (!o.TryGetValue("tests", out tok)) continue;
                    var tests = (JArray)tok;
                    foreach (var test in tests)
                    {
                        var oTest = (JObject)test;
                        var desc = (string)test["description"];
                        var input = (string)test["input"];
                        if (input.IndexOf("\\u", StringComparison.Ordinal) >= 0)
                        {
                            input = input.Replace("\\u0000", "\u0000");
                            input = input.Replace("\\u0009", "\u0009");
                            input = input.Replace("\\u000A", "\u000A");
                            input = input.Replace("\\u000B", "\u000B");
                            input = input.Replace("\\u000C", "\u000C");
                            input = input.Replace("\\uDFFF", "\uDFFF");
                            input = input.Replace("\\uD800", "\uD800");
                            if (input.IndexOf("\\u", StringComparison.Ordinal) >= 0)
                                throw new Exception();
                            //input = input.Replace("\\v", "\u000B");
                        }

                        var output = (JArray)test["output"];
                        JArray initialStates;
                        if (oTest.TryGetValue("initialStates", out tok))
                            initialStates = (JArray)tok;
                        else
                            initialStates = new JArray { "PCDATA state" };
                        Token.StartTag lastStartTag = null;
                        if (oTest.TryGetValue("lastStartTag", out tok))
                            lastStartTag = new Token.StartTag((string)tok);
                        foreach (var state in initialStates)
                        {
                            var tokenizer = new MyTokenizer2(input, (string)state, lastStartTag);
                            var res = tokenizer.Parse();
                            var message = desc + " " + Path.GetFileName(fname);
                            if (output.Count != res.Count)
                            {
                                //if (desc == "<\\u000B")
                                //{
                                //    // err, c1, err, c2 au lieu de err, err, c1c2
                                //    continue;
                                //}
                                Console.WriteLine("{0}: {1} != {2}", message, output.Count, res.Count);
                                if (res.Count < output.Count)
                                {
                                    Console.WriteLine();
                                }
                                if (/*Path.GetFileNameWithoutExtension(fname) == "test3" || */desc.Contains("lookahead region"))
                                {
                                    //continue;
                                }
                                //if (desc == "Invalid Unicode character U+DFFF with valid preceding character")
                                //{
                                //    // a, parse error, \ufffd au lieu de parse error, a\ufffd
                                //    continue;
                                //}
                                //Console.Write('!');
                            }
                            Assert.GreaterOrEqual(res.Count, output.Count, message);
                        }
                    }
                }
            }
        }
    }
}
