﻿using System;
using System.Collections.Generic;
using System.Text;
using HtmlParser5;
using NUnit.Framework;
using System.IO;

namespace tests
{
    [TestFixture]
    public class TestNamedCharRef
    {
        private static HtmlCookedBuffer CreateBuffer(string content)
        {
            return CreateBuffer(content, Encoding.UTF8);
        }

        private static HtmlCookedBuffer CreateBuffer(string content, Encoding encoding)
        {
            byte[] buf = encoding.GetBytes(content);
            var ms = new MemoryStream(buf);
            return new HtmlCookedBuffer(ms, encoding);
        }

        [Test]
        public void t_01()
        {
            HtmlCookedBuffer buffer = CreateBuffer("amp;");
            string s;
            int c;
            int c2;
            Assert.IsTrue(NamedCharRef.TryGetCharRef(buffer, out s, out c, out c2));
            Assert.AreEqual('&', (char)c);
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_02()
        {
            HtmlCookedBuffer buffer = CreateBuffer("amp;a");
            string s;
            int c;
            int c2;
            Assert.IsTrue(NamedCharRef.TryGetCharRef(buffer, out s, out c, out c2));
            Assert.AreEqual('&', (char)c);
            Assert.AreEqual('a', (char)buffer.Get());
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_03()
        {
            HtmlCookedBuffer buffer = CreateBuffer("amp");
            string s;
            int c;
            int c2;
            Assert.IsTrue(NamedCharRef.TryGetCharRef(buffer, out s, out c, out c2));
            Assert.AreEqual('&', (char)c);
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_04()
        {
            HtmlCookedBuffer buffer = CreateBuffer("ampx");
            string s;
            int c;
            int c2;
            Assert.IsTrue(NamedCharRef.TryGetCharRef(buffer, out s, out c, out c2));
            Assert.AreEqual('&', (char)c);
            Assert.AreEqual('x', (char)buffer.Get());
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_05()
        {
            HtmlCookedBuffer buffer = CreateBuffer("notit;");
            string s;
            int c;
            int c2;
            Assert.IsTrue(NamedCharRef.TryGetCharRef(buffer, out s, out c, out c2));
            Assert.AreEqual('¬', (char)c);
            Assert.AreEqual('i', (char)buffer.Get());
            Assert.AreEqual('t', (char)buffer.Get());
            Assert.AreEqual(';', (char)buffer.Get());
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_06()
        {
            HtmlCookedBuffer buffer = CreateBuffer("notin;");
            string s;
            int c;
            int c2;
            Assert.IsTrue(NamedCharRef.TryGetCharRef(buffer, out s, out c, out c2));
            Assert.AreEqual('∉', (char)c);
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_07()
        {
            HtmlCookedBuffer buffer = CreateBuffer("z");
            string s;
            int c;
            int c2;
            Assert.IsFalse(NamedCharRef.TryGetCharRef(buffer, out s, out c, out c2));
            Assert.AreEqual('z', (char)buffer.Get());
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        [Test]
        public void t_08()
        {
            HtmlCookedBuffer buffer = CreateBuffer("");
            string s;
            int c;
            int c2;
            Assert.IsFalse(NamedCharRef.TryGetCharRef(buffer, out s, out c, out c2));
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }

        // NotEqualTilde;
        [Test]
        public void t_09()
        {
            HtmlCookedBuffer buffer = CreateBuffer("NotEqualTilde;");
            string s;
            int c;
            int c2;
            Assert.IsTrue(NamedCharRef.TryGetCharRef(buffer, out s, out c, out c2));
            Assert.AreEqual('\u2242', (char)c);
            Assert.AreEqual('\u0338', (char)c2);
            Assert.AreEqual(HtmlCookedBuffer.EOF, buffer.Get());
        }
    }
}
