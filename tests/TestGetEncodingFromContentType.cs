﻿using System;
using System.Collections.Generic;
using System.Text;
using TswBase;
using HtmlParser5;
using NUnit.Framework;

namespace tests
{
    [TestFixture]
    public class TestGetEncodingFromContentType
    {
        [Test]
        public void t01()
        {
            string ct = null;
            Encoding e = Encoding.ASCII;
            Assert.AreEqual(e, EncodingFinder.GetEncodingFromContentType((SubString)ct, e));
        }

        [Test]
        public void t02()
        {
            SubString ct = SubString.Empty;
            Encoding e = Encoding.ASCII;
            Assert.AreEqual(e, EncodingFinder.GetEncodingFromContentType(ct, e));
        }

        [Test]
        public void t03()
        {
            SubString ct = (SubString)"text/html";
            Encoding e = Encoding.ASCII;
            Assert.AreEqual(e, EncodingFinder.GetEncodingFromContentType(ct, e));
        }

        [Test]
        public void t04()
        {
            SubString ct = (SubString)"text/html; charset";
            Encoding e = Encoding.ASCII;
            Assert.AreEqual(e, EncodingFinder.GetEncodingFromContentType(ct, e));
        }

        [Test]
        public void t05()
        {
            SubString ct = (SubString)"text/html; CHARSET";
            Encoding e = Encoding.ASCII;
            Assert.AreEqual(e, EncodingFinder.GetEncodingFromContentType(ct, e));
        }

        [Test]
        public void t06()
        {
            SubString ct = (SubString)"text/html; charset=";
            Encoding e = Encoding.ASCII;
            Assert.AreEqual(e, EncodingFinder.GetEncodingFromContentType(ct, e));
        }

        [Test]
        public void t07()
        {
            SubString ct = (SubString)"text/html; charset=null";
            Encoding e = Encoding.ASCII;
            Assert.AreEqual(e, EncodingFinder.GetEncodingFromContentType(ct, e));
        }

        [Test]
        public void t08()
        {
            SubString ct = (SubString)"text/html; charset=ascii";
            Encoding e = Encoding.ASCII;
            Assert.AreEqual(e, EncodingFinder.GetEncodingFromContentType(ct, null));
        }

        [Test]
        public void t09()
        {
            SubString ct = (SubString)"text/html; charset = ascii";
            Encoding e = Encoding.ASCII;
            Assert.AreEqual(e, EncodingFinder.GetEncodingFromContentType(ct, null));
        }

        [Test]
        public void t10()
        {
            SubString ct = (SubString)"text/html; charset\t  =ascii";
            Encoding e = Encoding.ASCII;
            Assert.AreEqual(e, EncodingFinder.GetEncodingFromContentType(ct, null));
        }

        [Test]
        public void t11()
        {
            SubString ct = (SubString)"text/html; charset   =\tascii    ";
            Encoding e = Encoding.ASCII;
            Assert.AreEqual(e, EncodingFinder.GetEncodingFromContentType(ct, null));
        }

        [Test]
        public void t12()
        {
            SubString ct = (SubString)"text/html; charset=ascii ";
            Encoding e = Encoding.ASCII;
            Assert.AreEqual(e, EncodingFinder.GetEncodingFromContentType(ct, null));
        }

        [Test]
        public void t13()
        {
            SubString ct = (SubString)"text/html; charset = ASCII ";
            Encoding e = Encoding.ASCII;
            Assert.AreEqual(e, EncodingFinder.GetEncodingFromContentType(ct, null));
        }

        [Test]
        public void t14()
        {
            SubString ct = (SubString)"text/html; charset=utf-8";
            Encoding e = Encoding.UTF8;
            Assert.AreEqual(e, EncodingFinder.GetEncodingFromContentType(ct, null));
        }

        [Test]
        public void t15()
        {
            SubString ct = (SubString)"text/html; charset=utf8";
            Encoding e = Encoding.UTF8;
            Assert.AreEqual(e, EncodingFinder.GetEncodingFromContentType(ct, null));
        }

        [Test]
        public void t16()
        {
            SubString ct = (SubString)"text/html; charset='utf-8'";
            Encoding e = Encoding.UTF8;
            Assert.AreEqual(e, EncodingFinder.GetEncodingFromContentType(ct, null));
        }

        [Test]
        public void t17()
        {
            SubString ct = (SubString)"text/html; charset='utf-8";
            Encoding e = Encoding.UTF8;
            Assert.AreEqual(null, EncodingFinder.GetEncodingFromContentType(ct, null));
        }

        [Test]
        public void t18()
        {
            SubString ct = (SubString)"text/html; charset=  'utf-8' ";
            Encoding e = Encoding.UTF8;
            Assert.AreEqual(e, EncodingFinder.GetEncodingFromContentType(ct, null));
        }

        [Test]
        public void t19()
        {
            SubString ct = (SubString)"text/html; charset=\"utf-8\"";
            Encoding e = Encoding.UTF8;
            Assert.AreEqual(e, EncodingFinder.GetEncodingFromContentType(ct, null));
        }

        [Test]
        public void t20()
        {
            SubString ct = (SubString)"charset=\"utf-8\"";
            Encoding e = Encoding.UTF8;
            Assert.AreEqual(e, EncodingFinder.GetEncodingFromContentType(ct, null));
        }

        [Test]
        public void t21()
        {
            SubString ct = (SubString)"application/x-java\tcharset=\"utf-8\"";
            Encoding e = Encoding.UTF8;
            Assert.AreEqual(e, EncodingFinder.GetEncodingFromContentType(ct, null));
        }

    }
}
