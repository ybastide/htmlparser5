﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using HtmlParser5;
using NUnit.Framework;

namespace tests
{
    [TestFixture]
    public class TestHtmlBufferDecoder
    {
        HtmlBuffer CreateBuffer(string text, Encoding encoding)
        {
            return CreateBuffer(text, encoding, encoding);
        }
        /// <summary>
        /// Creates a buffer from the given text.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="encoderEncoding">Encoding for filling the buffer.</param>
        /// <param name="decoderEncoding">Encoding for reading the buffer.</param>
        /// <returns></returns>
        HtmlBuffer CreateBuffer(string text, Encoding encoderEncoding, Encoding decoderEncoding)
        {
            byte[] b = encoderEncoding.GetBytes(text);
            HtmlSimpleBuffer hsb = new HtmlSimpleBuffer(b, b.Length);
            return new HtmlBuffer(hsb, decoderEncoding);
        }

        [Test]
        public void t_00_empty()
        {
            HtmlBuffer hb = CreateBuffer("", Encoding.ASCII);
            Assert.AreEqual(HtmlBuffer.EOF, hb.Peek());
            Assert.AreEqual(HtmlBuffer.EOF, hb.Get());
        }

        [Test]
        public void t_01_get_peek()
        {
            HtmlBuffer hb = CreateBuffer("a", Encoding.ASCII);
            Assert.AreEqual('a', hb.Peek());
            Assert.AreEqual('a', hb.Get());
            Assert.AreEqual(HtmlBuffer.EOF, hb.Peek());
            Assert.AreEqual(HtmlBuffer.EOF, hb.Get());
        }

        [Test]
        public void t_02_get_peek()
        {
            HtmlBuffer hb = CreateBuffer("abc", Encoding.ASCII);
            Assert.AreEqual('a', hb.Peek());
            Assert.AreEqual('a', hb.Get());
            Assert.AreEqual('b', hb.Get());
            Assert.AreEqual('c', hb.Get());
            Assert.AreEqual(HtmlBuffer.EOF, hb.Peek());
            Assert.AreEqual(HtmlBuffer.EOF, hb.Get());
        }

        [Test]
        public void t_03_put()
        {
            HtmlBuffer hb = CreateBuffer("", Encoding.ASCII);
            hb.Put('x');
            Assert.AreEqual('x', hb.Peek());
        }

        [Test]
        public void t_04_put()
        {
            HtmlBuffer hb = CreateBuffer("", Encoding.ASCII);
            hb.Put('x');
            Assert.AreEqual('x', hb.Peek());
            Assert.AreEqual('x', hb.Peek());
        }

        [Test]
        public void t_05_put()
        {
            HtmlBuffer hb = CreateBuffer("", Encoding.ASCII);
            hb.Put('x');
            Assert.AreEqual('x', hb.Get());
            Assert.AreEqual(HtmlBuffer.EOF, hb.Peek());
        }

        [Test]
        public void t_06_put()
        {
            HtmlBuffer hb = CreateBuffer("abc", Encoding.ASCII);
            Assert.AreEqual('a', hb.Peek());
            hb.Put('x');
            Assert.AreEqual('x', hb.Peek());
            Assert.AreEqual('x', hb.Get());
            Assert.AreEqual('a', hb.Get());
        }

        [Test]
        public void t_07_long()
        {
            StringBuilder sb = new StringBuilder(new string('a', HtmlBuffer.BufferSize));
            sb.Append('b');
            HtmlBuffer hb = CreateBuffer(sb.ToString(), Encoding.ASCII);
            for (int i = 0; i < HtmlBuffer.BufferSize; i++)
                Assert.AreEqual('a', hb.Get());
            Assert.AreEqual('b', hb.Get());
            Assert.AreEqual(HtmlBuffer.EOF, hb.Get());
        }

        [Test]
        public void t_08_latin1()
        {
            HtmlBuffer hb = CreateBuffer("é", Encoding.GetEncoding("iso-8859-1"));
            Assert.AreEqual('é', hb.Get());
            Assert.AreEqual(HtmlBuffer.EOF, hb.Get());
        }

        [Test]
        public void t_09_cp1252()
        {
            HtmlBuffer hb = CreateBuffer("€", Encoding.GetEncoding(1252), Encoding.GetEncoding("iso-8859-1"));
            Assert.AreEqual('€', hb.Get());
            Assert.AreEqual(HtmlBuffer.EOF, hb.Get());
        }

        [Test]
        public void t_10_latin9()
        {
            HtmlBuffer hb = CreateBuffer("€", Encoding.GetEncoding("iso-8859-15"));
            Assert.AreEqual('€', hb.Get());
            Assert.AreEqual(HtmlBuffer.EOF, hb.Get());
        }

        [Test]
        public void t_11_utf8()
        {
            HtmlBuffer hb = CreateBuffer("a", Encoding.UTF8);
            Assert.AreEqual('a', hb.Get());
            Assert.AreEqual(HtmlBuffer.EOF, hb.Get());
        }

        public void test_utf8_1_3(char c, int padding)
        {
            HtmlBuffer hb;
            StringBuilder sb = new StringBuilder();
            if (padding > 0)
                sb.Append(new string('x', padding));
            sb.Append(c);
            hb = CreateBuffer(sb.ToString(), Encoding.UTF8);
            for (int i = 0; i < padding; i++)
                Assert.AreEqual('x', hb.Get());
            Assert.AreEqual(c, hb.Get());
            Assert.AreEqual(HtmlBuffer.EOF, hb.Get());
        }

        public void test_utf8_4(byte[] u8, int u32, int padding)
        {
            HtmlBuffer hb;
            byte[] buffer = new byte[padding + u8.Length];
            for (int i = 0; i < padding; i++)
                buffer[i] = (byte)'x';
            Buffer.BlockCopy(u8, 0, buffer, padding, u8.Length);
            hb = new HtmlBuffer(new HtmlSimpleBuffer(buffer, buffer.Length), Encoding.UTF8);
            for (int i = 0; i < padding; i++)
                Assert.AreEqual('x', hb.Get());
            Assert.AreEqual(u32, hb.Get());
            Assert.AreEqual(HtmlBuffer.EOF, hb.Get());
        }

        [Test]
        public void t_12_utf8()
        {
            test_utf8_1_3('é', 0);
        }

        [Test]
        public void t_13_utf8()
        {
            test_utf8_1_3('€', 0);
        }

        [Test]
        public void t_14_utf8()
        {
            test_utf8_4(new byte[] { 0xF0, 0xA4, 0xAD, 0xA2 }, 0x24B62, 0);
        }

        [Test]
        public void t_15_utf8()
        {
            test_utf8_1_3('é', 511);
        }

        [Test]
        public void t_16_utf8()
        {
            test_utf8_1_3('€', 511);
        }

        [Test]
        public void t_17_utf8()
        {
            test_utf8_4(new byte[] { 0xF0, 0xA4, 0xAD, 0xA2 }, 0x24B62, 510);
        }

        [Test]
        public void t_18_utf8()
        {
            test_utf8_1_3('€', 511);
        }

        [Test]
        public void t_19_utf8()
        {
            test_utf8_4(new byte[] { 0xF0, 0xA4, 0xAD, 0xA2 }, 0x24B62, 510);
        }

        [Test]
        public void t_20_utf8()
        {
            test_utf8_4(new byte[] { 0xF0, 0xA4, 0xAD, 0xA2 }, 0x24B62, 509);
        }
    }
}
